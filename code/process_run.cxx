/** ****************************************************************
 *
 *  @abstract   macro to process a file taken with the LeCory scope
 *               - using nEXO group waveform code
 *  @author     Lennart Huth <huth@physi.uni-heidelberg.de>
 *  @date       Sep 2014
 *
 *
 *
 *
 ** ****************************************************************/



#include <iostream>
#include <string>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fstream>
#include <istream>

//root libraries
#include <TFile.h>
#include <TTree.h>
#include <TH1F.h>
#include <TCanvas.h>
#include "TF1.h"
#include "TNtuple.h"

using namespace std;

// helper structs
typedef struct{
    double falltime;
    double risetime;
}time_const;

typedef struct{
    int mRun;
    char mSetupFileName[200];
    char mFileName[200];
    double mWFAmpBining;
    double mAmpMin;
    double mAmpMax;
    double mNoise1Bin;
    double mDiffAmpThresh;
    double mRiseTimeSig;
    double mFallTimeTau;
    double mFallTimeTau2;
    int mPolarity;
    double mMinChi2ForRefit;
    double mMaxSPTemplateChi2;
}runparameter;


// functions used
void process_data(int runnumber, int method, int maxrun);
void get_file_name(char * FileInclPath, char * FileName);
double find_baseline(int runnumber, char * runlist);
time_const find_times(int runnumber, const char * runlist);
void change_baseline(double new_baseline, int run, const char * runlist);
void change_pulseshape_parameter(char * runlist, int run, double frt, double fft);

// main executable - requires just the runnumber as input
int main(int argc, char * argv[])

{
    int run = argc > 1 ?atoi(argv[1]) : -1;


    char runlist[200];
    sprintf(runlist,"RunInfo.txt");

    // perform the first processing

    process_data(run,0,100);
    cout << "limit" << endl;
    // find the baseline
    double baseline = find_baseline(run, runlist);
    cout << baseline << endl;
    //write it to the runfile
    change_baseline(baseline, run, runlist);

    //process again to get access to the rise and fall time defaults...
    process_data(run,2,10000);

    //correct rise and falltime in file
    time_const timeConst = find_times(run, runlist);
    change_pulseshape_parameter(runlist, run, timeConst.risetime, timeConst.falltime);
    //final processing
    process_data(run,101,1000);
}

void process_data(int runnumber, int method, int maxrun)
{
    char executable[100];
    sprintf(executable, "./bin/PulseFinding.exe %i %i %i 0"
            ,runnumber,method,maxrun);
    cout << executable << endl;
    system(executable);


}

void get_file_name(char * FileInclPath, char * FileName,int method)
{
    //>>> Strip directory name from file name
    int slashIndex=0;
    for(int index=0; index<strlen(FileInclPath); index++){
        if(strncmp(FileInclPath+index,"/",1)==0) slashIndex=index;
    }
    sprintf(FileName,"ntp%s.fanat%i",FileInclPath+slashIndex,method);

}


double find_baseline(int runnumber, char * runlist)
{
    //get filename corresponding to run
    fstream runFile(runlist);
    runparameter par;
    char buf[200];
    int last_run =-1;
    for(int iBuf=0; iBuf<11; iBuf++) runFile >> buf;
    runFile >> par.mRun >> par.mFileName >>  par.mWFAmpBining >> par.mAmpMin
            >> par.mAmpMax >> par.mNoise1Bin >> par.mRiseTimeSig >> par.mFallTimeTau
            >> par.mFallTimeTau2 >> par.mPolarity >> par.mMinChi2ForRefit;
    while(!runFile.eof() && par.mRun!=runnumber){
        runFile >> par.mRun >> par.mFileName >>  par.mWFAmpBining >> par.mAmpMin
                >> par.mAmpMax >> par.mNoise1Bin >> par.mRiseTimeSig
                >> par.mFallTimeTau2 >> par.mFallTimeTau >> par.mPolarity >> par.mMinChi2ForRefit;
        if(runFile.eof() && par.mRun!= runnumber){
            std::cerr << "find baseline: Could not find run requested " << runnumber << std::endl;
            exit(0);
        }
    }
    runFile.close();
    cout << "File exists!" <<endl;
    char tmp_file[200];
    get_file_name(par.mFileName,tmp_file,0);
    TFile file(tmp_file,"READ");
    TCanvas *c1 = new TCanvas("c1","c1",1600,900);

    TH1F *HBaseline = (TH1F* ) file.Get("HBaseline");
    HBaseline->Draw();

    // make plot nicer?

    TF1 * f1 = new TF1("gauss", "[0] / (sqrt(2.0 * TMath::Pi()) * [2]) * exp(-(x-[1])*(x-[1])/2./[2]/[2])", 0.04, 0.08);
    f1->SetParNames("Constant","Mean","Sigma");
    f1->SetParameters(2,0.06,0.002);
    f1->SetLineWidth(2);
    f1->SetLineColor(2);
    HBaseline->Fit(f1,"U");
    HBaseline->Draw();
    //gStyle->SetOptFit(1);
    char saveFile[200];
    sprintf(saveFile,"pulseFinderPlots/baseline_run%i.pdf",runnumber);
    c1->SaveAs(saveFile);
    char reFit[200];
//    sprintf(reFit,"xpdf %s &",saveFile);
    sprintf(reFit,"okular %s &",saveFile);
    int goodfit = -1;
    double amplitude,mean,sigma;
    while(goodfit!=1)
    {
      system(reFit);
      cout << "Fit okay? If so enter 1, else 0" << endl;
      cin >> goodfit;
      if(goodfit!=1)
      {
	cout << "set new initial values:\n Amplitude: ";
	cin >> amplitude ;
	cout << endl << "Mean: ";
	cin >> mean;
	cout << endl << "Sigma: ";
	cin >> sigma;
	f1->SetParameters(amplitude, mean, sigma);
	HBaseline->Fit(f1,"U");
	HBaseline->Draw();
	sprintf(saveFile,"pulseFinderPlots/baseline_run%i.pdf",runnumber);
	c1->SaveAs(saveFile);
      }
      else{
	cout << "Fit accepted!\n";
      }
    }
    
    return f1->GetParameter("Sigma");


}


time_const find_times(int runnumber, const char * runlist)
{
    time_const tc;

    //get filename corresponding to run
    fstream runFile(runlist);
    runparameter par;
    char buf[200];
    int last_run =-1;
    for(int iBuf=0; iBuf<11; iBuf++) runFile >> buf;
    runFile >> par.mRun >> par.mFileName >>  par.mWFAmpBining >> par.mAmpMin
            >> par.mAmpMax >> par.mNoise1Bin >> par.mRiseTimeSig >> par.mFallTimeTau
            >> par.mFallTimeTau2  >> par.mPolarity >> par.mMinChi2ForRefit;
    while(!runFile.eof() && par.mRun!=runnumber){
        runFile >> par.mRun >> par.mFileName >>  par.mWFAmpBining >> par.mAmpMin
                >> par.mAmpMax >> par.mNoise1Bin >> par.mRiseTimeSig
                >> par.mFallTimeTau >> par.mFallTimeTau2 >> par.mPolarity >> par.mMinChi2ForRefit;
        if(runFile.eof() && par.mRun!= runnumber){
            std::cerr << "Find times: Could not find run requested " << runnumber << std::endl;
            exit(0);
        }
    }
    runFile.close();

    char tmp_file[200];
    get_file_name(par.mFileName,tmp_file,2);
    TFile file(tmp_file,"READ");
    TCanvas *c1 = new TCanvas("c1","c1",1600,900);

    //cout << "Opening File: " << tmp_file << endl;
    TNtuple *ntp = (TNtuple*) file.Get("ntp");

    // save fa to identify spe
    char saveFile[200];
    sprintf(saveFile,"pulseFinderPlots/fa_run%i.pdf",runnumber);
    ntp->Draw("fa>>h(200,-0.2,0.01)");
    c1->SetLogy(1);
    c1->SaveAs(saveFile);

    // check for fa
    char plotfile[200];
    sprintf(plotfile,"okular pulseFinderPlots/fa_run%i.pdf &",runnumber);
    system(plotfile);
    double upperLimit = -1,lowerLimit = -1;
    cout << "Enter the right Limit of the SPR Peak:" << endl;
    cin >> upperLimit;
    cout << "Enter the left Limit of the SPR Peak:" << endl;
    cin >> lowerLimit;

    char SPEcut[200];
    sprintf(SPEcut,"fa > %f && fa< %f", lowerLimit, upperLimit);
    ntp->Draw("fft>>h(200,-1e-9,5e-8)",SPEcut,"");
    sprintf(saveFile,"pulseFinderPlots/fft_run%i.pdf",runnumber);
    c1->SetLogy(0);
    c1->SaveAs(saveFile);


    ntp->Draw("frt>>h(200,-0.01e-9,5e-9)",SPEcut,"");
    sprintf(saveFile,"pulseFinderPlots/frt_run%i.pdf",runnumber);
    c1->SaveAs(saveFile);

    delete c1;
    file.Close();


    //sprintf(plotfile,"xpdf pulseFinderPlots/fft_run%i.pdf &",runnumber);
    sprintf(plotfile,"okular pulseFinderPlots/fft_run%i.pdf &",runnumber);
    system(plotfile);

    cout << "DEFAULTS: FFT " << par.mFallTimeTau << "  FRT " << par.mRiseTimeSig << endl;
    tc.falltime = -1;
    tc.risetime = -1;
    while(tc.falltime <= 0)
    {
        cout << "Enter correct falltime!" << endl;
        cin >> tc.falltime;
        if(tc.falltime <=0)
        {
            cout << "Can you also walk back in time? - falltime has to be larger 0" << endl;
        }
    }
    //sprintf(plotfile,"xpdf pulseFinderPlots/frt_run%i.pdf &",runnumber);
    sprintf(plotfile,"okular pulseFinderPlots/frt_run%i.pdf &",runnumber);
    system(plotfile);
    while(tc.risetime < 0)
    {
        cout << "Enter correct risetime!" << endl;
        cin >> tc.risetime;
        if(tc.risetime <=0)
        {
            cout << "Can you also walk back in time? - risetime has to be larger 0" << endl;
        }
    }
    return tc;


}


void change_baseline(double new_baseline, int run, const char * runlist)
{

    ofstream writeFile;
    writeFile.open(("tmp.txt"));
    ifstream runFile(runlist);

    runparameter par;
    char buf[200];
    int last_run =-1;
    for(int iBuf=0; iBuf<11; iBuf++)
    {
        runFile >> buf;
        writeFile << buf << "\t";

    }
    writeFile << endl;
    runFile >> par.mRun >> par.mFileName >>  par.mWFAmpBining >> par.mAmpMin
            >> par.mAmpMax >> par.mNoise1Bin >> par.mRiseTimeSig >> par.mFallTimeTau
            >> par.mFallTimeTau2 >> par.mPolarity >> par.mMinChi2ForRefit;



    while(!runFile.eof()){
        runFile >> par.mRun  >> par.mFileName  >>  par.mWFAmpBining  >> par.mAmpMin
                 >> par.mAmpMax  >> par.mNoise1Bin  >> par.mRiseTimeSig
                 >> par.mFallTimeTau   >> par.mFallTimeTau2
                 >> par.mPolarity  >> par.mMinChi2ForRefit;
        if(par.mRun==run)
        {
            par.mNoise1Bin = new_baseline;

        }
        else
        {
            //nothing to do, just write again
        }

        writeFile << par.mRun << " " << par.mFileName<< " " <<  par.mWFAmpBining<< " " << par.mAmpMin
                  << " "<< par.mAmpMax << " "<< par.mNoise1Bin << " "<< par.mRiseTimeSig
                  << " "<< par.mFallTimeTau <<" "<< par.mFallTimeTau2 << " "<< par.mPolarity << " "<< par.mMinChi2ForRefit<<endl;

    }
    runFile.close();
    writeFile.close();
    char mv_files[200];
    sprintf(mv_files,"mv tmp.txt %s",runlist);
    system(mv_files);
}


void change_pulseshape_parameter(char * runlist, int run, double frt, double fft)
{
    ofstream writeFile;
    writeFile.open(("tmp.txt"));
    ifstream runFile(runlist);

    runparameter par;
    char buf[200];
    int last_run =-1;
    for(int iBuf=0; iBuf<11; iBuf++)
    {
        runFile >> buf;
        writeFile << buf << "\t";

    }
    writeFile << endl;
    runFile >> par.mRun >> par.mFileName >>  par.mWFAmpBining >> par.mAmpMin
            >> par.mAmpMax >> par.mNoise1Bin >> par.mRiseTimeSig >> par.mFallTimeTau
            >> par.mFallTimeTau2  >> par.mPolarity >> par.mMinChi2ForRefit;



    while(!runFile.eof()){
        runFile >> par.mRun  >> par.mFileName  >>  par.mWFAmpBining  >> par.mAmpMin
                 >> par.mAmpMax  >> par.mNoise1Bin  >> par.mRiseTimeSig
                 >> par.mFallTimeTau >> par.mFallTimeTau2   >> par.mPolarity  >> par.mMinChi2ForRefit;
        if(par.mRun==run)
        {
            par.mRiseTimeSig = frt;
            par.mFallTimeTau = fft;
        }
        else
        {
            //nothing to do, just write again
        }

        writeFile << par.mRun << " " << par.mFileName<< " " <<  par.mWFAmpBining<< " " << par.mAmpMin
                  << " "<< par.mAmpMax << " "<< par.mNoise1Bin << " "<< par.mRiseTimeSig
                  << " "<< par.mFallTimeTau <<" "<< par.mFallTimeTau2 << " "<< par.mPolarity << " "<< par.mMinChi2ForRefit<<endl;

    }
    runFile.close();
    writeFile.close();
    char mv_files[200];
    sprintf(mv_files,"mv tmp.txt %s",runlist);
    system(mv_files);

}



