/**
*
*	Main mulitthread data taking executable:
*   Controls interlock and readsout data
*	@author Lennart Huth <huth@physi.uni-heidelberg.de>
*	@date 	14 Oct 2014
*
**/


#include <iostream>
#include <string>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fstream>
#include <istream>
#include <thread>
#include <unistd.h>
#include "Keithleymscb.hpp"
//#include <atomic>
using namespace std;



//atomic <bool> test (false);
bool test = false;
bool t1()
{
    cout << "t1 start" << endl;
    test = true;
    cout << "t1 end" << endl;

    return true;
}

void t2()
{

        cout << "t2" << endl;
        

}


int main(int argc, char * argv[])
{
/*
  // This is a really ugly solution, but it is working
    // on ladd20 some issues...
    
    std::thread t(t1);
    while(!test)
    {
        t2();
    }

    // keithley supply
    //KeithleyMSCB keithley("da",2,"di", 2, 3);
    string str("./../packages/mscb/msc -d mscb502 -a 1 -c \"w 5 CONF:CURR\"");
    system(str.c_str());
    str = ("./../packages/mscb/msc -d mscb502 -a 1 -c \"w 5 MEAS?\"");
    system(str.c_str());
    str = ("./../packages/mscb/msc -d mscb502 -a 1 -c \"r 6\"");
    system(str.c_str());
    
*/
    //Temperature MSCB
    TemperatureMSCB tempControl("da",1,"di",1);
    
    
    
    RemotePower powerBar;
    //powerBar.print();
    
    powerBar.turn_on(1);
    powerBar.turn_on(2);
    float t = tempControl.read_temp();
    
    while(true)
    {
      t = tempControl.read_temp();
      if(t>=35)
      {
	powerBar.turn_off(2);
	powerBar.turn_off(1);
	break;
      }
      sleep(2);
    }
    
    
//    thread Interlock();
//    thread datataking();

}









