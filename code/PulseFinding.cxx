/*******************************************************************************
* Distribution.cxx
*
* Description:
* Reads a wfm file and produces an ntuple with the distribution of the pulses
* for waveforms in that wfm file.
*
* History:
* v0.1  2011/12/14  Initial file (Kyle Boone, kyboone@gmail.com)
*******************************************************************************/

#include <sys/stat.h>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <time.h>
#include "LecroyFile.h"
#include "WaveformProcessor.h"

#include "TFile.h"
#include "TNtuple.h"
#include "TF1.h"
#include "TH1.h"

// Parameters

int main(int argc, char** argv){
  int aRun = argc>1 ? atoi(argv[1]) : 0;
  int aFitType = argc>2 ? atoi(argv[2]) : 0;
  int aNEventMax = argc>3 ? atoi(argv[3]) : 100000000;
  int _2TCFit = argc>4 ? atoi(argv[4]) : 0;
  WaveformProcessor wfProc("RunInfo.txt",aRun,5);
  std::cout << "dada" << std::endl;
  // --- Open output file
  //>>> Strip directory name from file name
  int slashIndex=0;
  for(int index=0; index<strlen(wfProc.getFileName()); index++){
    if(strncmp(wfProc.getFileName()+index,"/",1)==0) slashIndex=index;
  }
  char outFileName[200];
  sprintf(outFileName,"ntp/%s.fanat%i",wfProc.getFileName()+slashIndex,aFitType);
  //sprintf(outFileName,"/home/huth/Desktop/nEXO/testdata%s.fanat%i",wfProc.getFileName()+slashIndex,aFitType);
  std::cout << "PulseFinding " << outFileName <<  " " << wfProc.getBaselineRMS() << slashIndex << std::endl;

  bool tc;
  if(_2TCFit == 1)
      tc = true;
  else tc = false;


  TFile outFile(outFileName ,"RECREATE");
  TNtuple ntp("ntp","ntp",
          "evt:tt:blmu:blRMS:np:pa:pt:tchi2:fa:ft:frt:fft:fft2:fblmu:fchi2:ndf:frchi2:frndf:pr:nl:paa:pq:pw");
  float ntpCont[22];
  int skippedcount = 0;

  time_t t;
  time(&t);
  int t0 = t;
  // ---
  int nEvent = wfProc.getWaveformCount();
  if(nEvent>aNEventMax) nEvent=aNEventMax;
  for(int iEvent=0; iEvent<nEvent; iEvent++){
    wfProc.readNextWaveform();
    if(wfProc.processBaseline() && wfProc.findPulse()){
      wfProc.fit2(aFitType, tc);
//      std::cout << "Event: " << iEvent <<"\t nPulses: "
//                << wfProc.getNPulse() << std::endl;
      for(int iPulse=0; iPulse<wfProc.getNPulse(); iPulse++){
	ntpCont[0]=iEvent;
	ntpCont[1]=wfProc.getTriggerTime();
	ntpCont[2]=wfProc.getBaselineMu();
	ntpCont[3]=wfProc.getBaselineRMS();
	ntpCont[4]=wfProc.getNPulse();
	ntpCont[5]=wfProc.getPulseAmplitude(iPulse);
	ntpCont[6]=wfProc.getPulseTime(iPulse);
	ntpCont[7]=wfProc.getSPTemplateChi2(iPulse);
	ntpCont[8]=wfProc.getFitAmplitude(iPulse);
	ntpCont[9]=wfProc.getFitTime(iPulse);
    ntpCont[10]=wfProc.getFitRiseTime(iPulse);
    ntpCont[11]=wfProc.getFitFallTime(iPulse);
    ntpCont[12]=wfProc.getFitFallTime2(iPulse);
    ntpCont[13]=wfProc.getFitBaseline(iPulse);
    ntpCont[14]=wfProc.getChi2(iPulse);
    ntpCont[15]=wfProc.getNDF(iPulse);
    ntpCont[16]=wfProc.getChi2Refit(iPulse);
    ntpCont[17]=wfProc.getNDFRefit(iPulse);
    ntpCont[18]=wfProc.getPulseRatio(iPulse);
    ntpCont[19]=wfProc.checkNoise(); // noise lvl check -> used for debugging only at the moment
    ntpCont[20]=wfProc.getPulseAbsAmplitude(iPulse);
    ntpCont[21]=wfProc.getPulseCharge(iPulse);
    ntpCont[22]=wfProc.getPulseWidth(iPulse);

	ntp.Fill(ntpCont);
      }
    }
    else{
      skippedcount++;
      ntpCont[0]=iEvent;
      ntpCont[1]=wfProc.getTriggerTime();
      ntpCont[2]=wfProc.getBaselineMu();
      ntpCont[3]=wfProc.getBaselineRMS();
      ntpCont[4]=-1;
      ntp.Fill(ntpCont);
    }
    if(iEvent==0) wfProc.getBaselineHisto()->Write();
    if(floor(iEvent*200./nEvent)==(iEvent*200./nEvent)){
        time(&t);
        std::cout << iEvent << "/" << nEvent << " running for " << (t-t0) << "s" <<std::endl; //"\r";
        //std::cout.flush();
    }
    //if(iEvent>195000) std::cout << iEvent << std::endl;
  }		

  std::cout << "Writing output in " << outFileName << std::endl;
  std::cout << "Skipped " << skippedcount << " triggers." << std::endl;
  ntp.Write();
}

