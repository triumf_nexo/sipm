#include "Keithleymscb.hpp"
using namespace std;

KeithleyMSCB::KeithleyMSCB(std::string mscbDevice,
                           const int node,
                           std::string mscbPath,
                           int writeAdd, int readAdd)
{
    _device     = mscbDevice;
    _node       = node;
    _mscbPath   = mscbPath;
    _writeAdd   = writeAdd;
    _readAdd    = readAdd;
    _execute    = _mscbPath;
    }

float KeithleyMSCB::set_voltage(float v)
{
    sleep(1);
    string str("./../packages/mscb/msc -d mscb502 -a 1 -c \"w 5 SOUR:VOLT 1\"");
    system(str.c_str());
    //str = send_command(str);
    str = ("./../packages/mscb/msc -d mscb502 -a 1 -c \"r 6\"");
    sleep(1);
    system(str.c_str());
    //str = send_command(str);
    cout << "final: "<<str << endl;
    
    // add correct word to search for here:
    string voltage = str.substr(str.find("Input:")+6,10);
    cout << voltage << endl;
    return atof(voltage.c_str());

}





float KeithleyMSCB::read_current()
{
    string str ("./../packages/mscb/msc -d mscb502 -a 1 -c \"w 5 MEAS?\"");
    system(str.c_str());
    //str = send_command(str);
    str = "./../packages/mscb/msc -d mscb502 -a 1 -c \"r 6\""	;
    system(str.c_str());
    //str = send_command(str);
    
    // add correct word to search for here:
    string current = str.substr(str.find(""),1);
    cout << current << endl;
    return atof(current.c_str());
}


float KeithleyMSCB::read_voltage()
{
    string str("./../packages/mscb/msc -d mscb502 -a 1 -c \"w 5 SOUR:VOLT?\"");
    //cout << str << endl;
    sleep(0.5);
    system(str.c_str());
    //str = send_command(str);
    str = ("./../packages/mscb/msc -d mscb502 -a 1 -c \"r 6\"");
    system(str.c_str());
    //str = send_command(str);
    //cout <<str << endl;
    
    // add correct word to search for here:
    string voltage = str.substr(str.find("Input:")+6,10);
    //cout << voltage << endl;
    return atof(voltage.c_str());
}


std::string KeithleyMSCB::send_command(std::string command)
{
    // source: http://stackoverflow.com/questions/478898/how-to-execute-a-command-and-get-output-of-command-within-c
    FILE* pipe = popen(command.c_str(), "r");
    if (!pipe) return "ERROR";
    char buffer[256];
    string result = "";
    while(!feof(pipe)) {
        if(fgets(buffer, 256, pipe) != NULL)
            result += buffer;
    }
    pclose(pipe);
    return result;
}


double TemperatureMSCB::read_temp()
{
//    change to variable input
    FILE* pipe = popen("./../packages/mscb/msc -d mscb502 -a 2 -c \"r 40\""
, "r");
    if (!pipe) return 50;
    char buffer[128];
    string result = "";
    while(!feof(pipe)) {
        if(fgets(buffer, 128, pipe) != NULL)
            result += buffer;
    }
    pclose(pipe);
    size_t temp = result.find("deg.");
    string t = result.substr(temp-7,6);
    cout << t << endl;
    return ((float) atof(t.c_str()));

}



void RemotePower::turn_off(int ch)
{
char buffer[200];
sprintf(buffer,"snmpset -v 2c -M +/home/exo/packages/cdu -m +Sentry3-MIB -c guru cdudaq01 outletControlAction.1.1.%i i 2",ch);
system(buffer);
}

void RemotePower::turn_on(int ch)
{
char buffer[200];
sprintf(buffer,"snmpset -v 2c -M +/home/exo/packages/cdu -m +Sentry3-MIB -c guru cdudaq01 outletControlAction.1.1.%i i 1",ch);
system(buffer);
}

void RemotePower::turn_onAll()
{
for(int i = 1; i<8;++i)
{
  turn_on(i);
}
}
void RemotePower::turn_offAll()
{
for(int i = 1; i<8;++i)
{
  turn_off(i);
}
}
