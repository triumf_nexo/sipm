/*******************************************************************************
* DarkNtpAnalysis.cxx
*
* Description:
* Analyzes an NTuple for dark noise, afterpulse probability, etc. Also provides
* formatting for histogram generation.
*
History:
* v0.1  2014/09/01  Initial file: 	Lloyd James, lloyd.thomas.james@gmail.com
                                    Nolan Ohmart, ohmartn@gmail.com
* v1.1 2014/09/21 File Totally Rewritten:	Lloyd James

* v1.2 2014/10/07 New fitfunction added to take multiple peaks into account
*                                   Lennart Huth <huth@physi.uni-heidelberg.de>

*******************************************************************************/

#include <sys/stat.h>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>

#include "TFile.h"
#include "TNtuple.h"
#include "TF1.h"
#include "TF2.h"
#include "TROOT.h"
#include "TH1.h"
#include "TH2.h"
#include "TGraph.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TSystem.h"
#include "TStyle.h"

#include "DarkNtpAnalysis.h"

using namespace std;
typedef struct
{
    double time;
    double ft;
    double ampl;
    double chi2;
    double tt;
    double uppeak;
    double pt;
    double tpt;
    double pw;
} pulse;
bool comp(const pulse &a, const pulse &b)
{
    return(a.pt<b.pt);
}
// set the values of the blind window and the cut for the pulsefinder

const double gapUpperBound = 2.e3,
gapLowerBound = 4.e3,
Starttime = 10.;
double  DeadTime = Starttime,
        GapStart = gapLowerBound,
        GapEnd = gapUpperBound;


DarkNtpAnalysis::DarkNtpAnalysis(int aRun)
{
    init(aRun);
}

//par[0] = tau, par[1] = p_ap, par[2] = tau_ap, par[3] = p_dn
double fitFunction(double *x, double *par)
{
    double fitval;
    if((par[1] != 0) && (par[3] != 0))
    {
        fitval = par[2]/par[3] * exp(-1*x[0]/par[3]) * (par[0]*exp(-1*x[0]/par[1]) - par[0] + 1) + par[0]/par[1] * exp(-1*x[0]/par[1]) * (par[2]*exp(-1*x[0]/par[3]) - par[2] + 1);
    }
    else fitval = 0;

    return fitval;
}


double funcDN(double* x, double* p){
  if(x[0]<DeadTime) return 0.;
  if(x[0]<GapStart){
    return p[0]*(1.-p[1])*exp(-p[0]*(x[0]-DeadTime));
  }
  else{
    if(x[0]<GapEnd) return 0.;
    return p[0]*(1.-p[1])*exp(-p[0]*(x[0]-DeadTime-GapEnd+GapStart));
  }
}

double funcDNAP1(double* x, double* p){
  // p[0] = dark noise rate
  // p[1] = <nap>
  // p[2] = tauAp
  if(x[0]<DeadTime) return 0.;
  if(x[0]<GapStart){
    double noAP=1.;
    double singleAP=0.;
    double Poisson;
    double expTau = exp(-x[0]/p[2]);
    double expDeadTime = exp(-DeadTime/p[2]);
    double factPar = 1.;
    for(int iAP=1; iAP<5; iAP++) {
      Poisson=TMath::Poisson(iAP,p[1]);
      singleAP += Poisson*(iAP==1? 1. : (1.-factPar)); // if(AP==1) return 1, else 1-factPar
      factPar*=(expDeadTime-expTau);
      noAP -= Poisson*factPar;
    }
    singleAP*=expTau/p[2];
    return exp(-p[0]*(x[0]-DeadTime))*(noAP*p[0]+singleAP);
  }

  else if(x[0]<GapEnd)
      return 0;

  else{
      double noAP=1.;
      double singleAP=0.;
      double Poisson;
      double expTau = exp((-x[0]-(-GapEnd+GapStart))/p[2]);
      double expDeadTime = exp(-DeadTime/p[2]);
      double factPar = 1.;

      for(int iAP=1; iAP<5; iAP++) {
        Poisson=TMath::Poisson(iAP,p[1]);
        singleAP += Poisson*(iAP==1? 1. : (1.-factPar));
        factPar*=(expDeadTime-expTau);
        noAP -= Poisson*factPar;
      }

      singleAP*=expTau/p[2];
      return exp(-p[0]*(x[0]-DeadTime-GapEnd+GapStart))*(noAP*p[0]+singleAP);
  }

  /*
  // old assumption of now after pusle after gap -> replaced by after-pulses after gap
  else{// assume no after-pulse after gap
    if(x[0]<GapEnd) return 0.;
    return (1.-p[1])*p[0]*exp(-p[0]*(x[0]-DeadTime-GapEnd+GapStart));
  }
  */
}

double funcDNAP2(double* x, double* p){
  // p[0] = dark noise rate
  // p[1] = <nap1>
  // p[2] = tauAp1
  // p[3] = <nap2>
  // p[4] = tauAp2
  if(x[0]<DeadTime) return 0.;
  if(x[0]<GapStart){
    double noAP1=1.;
    double singleAP1=0.;
    double Poisson;
    double expTau1 = exp(-x[0]/p[2]);
    double expDeadTime1 = exp(-DeadTime/p[2]);
    double factPar1 = 1.;
    double noAP2=1.;
    double singleAP2=0.;
    double expTau2 = exp(-x[0]/p[4]);
    double expDeadTime2 = exp(-DeadTime/p[4]);
    double factPar2 = 1.;
    for(int iAP=1; iAP<5; iAP++) {
      Poisson=TMath::Poisson(iAP,p[1]);
      singleAP1 += Poisson*(iAP==1? 1. : (1.-factPar1));
      factPar1*=(expDeadTime1-expTau1);
      noAP1 -= Poisson*factPar1;
      Poisson=TMath::Poisson(iAP,p[3]);
      singleAP2 += Poisson*(iAP==1? 1. : (1.-factPar2));
      factPar2*=(expDeadTime2-expTau2);
      noAP2 -= Poisson*factPar2;
    }
    singleAP1*=expTau1/p[2];
    singleAP2*=expTau2/p[4];
    return exp(-p[0]*(x[0]-DeadTime))*(noAP1*noAP2*p[0]+noAP2*singleAP1+noAP1*singleAP2);
  }
  else{// assume no after-pulse after gap
    if(x[0]<GapEnd) return 0.;
    return (1.-p[1])*p[0]*exp(-p[0]*(x[0]-DeadTime-GapEnd+GapStart));
  }
}

double funcDNAP2Rec(double* x, double* p){
  // p[0] = dark noise rate
  // p[1] = <nap1>
  // p[2] = tauAp1
  // p[3] = <nap2>
  // p[4] = tauAp2
  // p[5] = tauRec
  if(x[0]<DeadTime) return 0.;
  if(x[0]<GapStart){
    double noAP1=1.;
    double singleAP1=0.;
    double Poisson;
    double expTau1 = exp(-x[0]/p[2]);
    double expDeadTime1 = exp(-DeadTime/p[2]);
    double tauRecEff1 = p[2]*p[5]/(p[2]+p[5]);
    double expTauRec1 = exp(-x[0]/tauRecEff1);
    double expDeadTimeRec1 = exp(-DeadTime/tauRecEff1);
    double factPar1 = 1.;
    double noAP2=1.;
    double singleAP2=0.;
    double expTau2 = exp(-x[0]/p[4]);
    double expDeadTime2 = exp(-DeadTime/p[4]);
    double tauRecEff2 = p[4]*p[5]/(p[4]+p[5]);
    double expTauRec2 = exp(-x[0]/tauRecEff2);
    double expDeadTimeRec2 = exp(-DeadTime/tauRecEff2);
    double factPar2 = 1.;
    for(int iAP=1; iAP<5; iAP++) {
      Poisson=TMath::Poisson(iAP,p[1]);
      singleAP1 += Poisson*(iAP==1? 1. : (1.-factPar1));
      factPar1*=(expDeadTime1-expTau1-
         tauRecEff1/p[2]*(expDeadTimeRec1-expTauRec1));
      noAP1 -= Poisson*factPar1;
      Poisson=TMath::Poisson(iAP,p[3]);
      singleAP2 += Poisson*(iAP==1? 1. : (1.-factPar2));
      factPar2*=(expDeadTime2-expTau2-
         tauRecEff2/p[4]*(expDeadTimeRec2-expTauRec2));
      noAP2 -= Poisson*factPar2;
    }
    singleAP1*=expTau1/p[2]*(1.-exp(-x[0]/p[5]));
    singleAP2*=expTau2/p[4]*(1.-exp(-x[0]/p[5]));
    return exp(-p[0]*(x[0]-DeadTime))*(noAP1*noAP2*p[0]+noAP2*singleAP1+noAP1*singleAP2);
  }
  else{// assume no after-pulse after gap
    if(x[0]<GapEnd) return 0.;
    return (1.-p[1])*p[0]*exp(-p[0]*(x[0]-DeadTime-GapEnd+GapStart));
  }
}



/**
 *
 * Fit function to extract the darknoise rate and the afterpulsingprobability
 * Requieres normalized data
 */
// par[0] = tau_n, par[1] = tau_ap, par[2] = P_ap
double alt_fit(double *x, double *par)
{
    if(par[0] != 0 && par[1] != 0)
        return (((1.0 - par[2] * (1.0 - exp(-x[0]/par[1]))) * exp(-x[0]/par[0])/par[0])+ (par[2]/par[1] * exp(-x[0]/par[0]) * exp(-x[0]/par[1])));
    else return 1e-5;
}

// par[0] = tau_n
// par[1] = number of pulses
// par[2+2i] = tau_ap(i)
// par[3+2i] = P_ap(i)
double multiple_tc(double *x, double *par)
{
    double p_p = 0;
    double integral_pp =0;
    // n afterpulse tc's
    for(int tc=0; tc < par[1]; tc++)
    {
        if(par[2+2*tc] != 0)
        {
            p_p   += par[3+2*tc]/par[2+2*tc] *exp(-x[0]/par[2+2*tc]);
            integral_pp += par[3+2*tc]*(1- exp(-x[0]/par[2+2*tc]));
        }
        else
        {
            cerr << "wrong fit input" << endl;
        }
    }
    // darknoise
    double p_n = 0, integral_pn = 0;
    if(par[0]!=0)
    {

        p_n = ((exp(-x[0]/par[0]))/par[0]);
        integral_pn = (1 - exp(-x[0]/par[0]));
    }
    return (p_n * (1-integral_pp) +(1- integral_pn) * p_p);
}

// par[0] = tau, par[1] = normalization
double norm_exp(double *x, double *par)
{

    if(par[0]!=0)
    {
        // takes the gap into account!
        if(x[0] < gapLowerBound && x[0]>Starttime)
            return (par[1]*exp(-x[0]/par[0]));
        else if(x[0]>gapUpperBound)
            return (par[1]*exp((-(x[0]-(gapUpperBound-gapLowerBound)))/par[0]));
        else return 0;
    }
    else return 1;
}
void DarkNtpAnalysis::init(int run) //Reads data from config files, sets global variable values
{
    gSystem->Load("libTree"); //For some reason needed to open NTuples on my computer - may not be needed for others  ¯\_(ツ)_/¯

    mRun = run;

    //Opens a configuration file and reads the fit type, and the paths for reading and saving information.

    char configname[100];
    sprintf(configname, "./code/DarkAnalysis.config");
    ifstream iFile;
    iFile.open(configname);

    char buffer[100];

    //mFitType
    iFile >> buffer >> buffer >> buffer >> buffer;
    mFitType = atoi(buffer);

    //mRunInfo
    iFile >> buffer >> buffer >> buffer >> buffer;
    strRunInfo = buffer;
    mRunInfo = &strRunInfo[0];

    //mSavePath
    iFile >> buffer >> buffer >> buffer >> buffer;
    strSavePath = buffer;
    mSavePath = &strSavePath[0];

    //mXTalkSavePath
    iFile >> buffer >> buffer >> buffer >> buffer;
    strXTalkSavePath = buffer;
    mXTalkSavePath = &strXTalkSavePath[0];

    iFile.close();

    mFileName = getFileName();
    cout << "Opening " << mFileName <<" read from " << mRunInfo  << endl;
}

void DarkNtpAnalysis::createTimingHistogram()
{
    // some cut values for SPR selection, min delta t
    double SPE_up  = -0.022;
    double SPE_low = -0.043;
    double delt_min= 1e-8;

    //Reads NTuple from .fanat# file in ntp/
    TFile* tFile = new TFile(mFileName);
    TNtuple* ntp = (TNtuple*) tFile->Get("ntp");
    NtpCont* cont = (NtpCont*) ntp->GetArgs();

    int nEntries = ntp->GetEntries();
    int nBinMax=500;
    double bin[500];
    double upperBound=0.;
    int iBin=0;

    //Sets logarithmic lower bin limits
    while(upperBound<1.0e6 && iBin<nBinMax)
    {
        bin[iBin]=upperBound;
        upperBound = pow(10,iBin/10.);
        iBin++;
    }
    bin[iBin]=upperBound;

    TFile f("test.root","RECREATE");
    TimeDist = new TH1D("TimeDist","TimeDist",iBin,bin);
    vector<pulse> data;
    data.clear();
    // read in the ntuple
    int counter = 0;
    double tPrev=-1;
    int testcount = 0;
    int event = 0;
    ntp->GetEntry(counter);
    while(counter < nEntries)
    {
        pulse _evt;
        while(tPrev <= cont->tt && counter < nEntries) // add "&& event == cont->evt" in case you only want one waveform!
        {
            tPrev = cont->tt;
            event = cont->evt;
            _evt.time = cont->ft + cont->tt;
            _evt.ft = cont->ft;
            _evt.ampl = cont->fa;
            _evt.chi2 = cont->frchi2/cont->frndf;
            data.push_back(_evt);
            counter++;
            ntp->GetEntry(counter);
        }
        event = cont->evt;
        tPrev =-1;
        // sort the data
        sort(data.begin(), data.end(), comp);
        // Fill the histo with the time differences
        int i = 0;
        while(i<(data.size()-1))
        {


            // mother pulse should be a SPE and at the trigger time to keep the dead window sharp
            //cout << counter << "\t" << i << "\t" << data.size() << endl;
            if(data[i].ampl < SPE_up && data[i].ampl > SPE_low && abs(data[i].ft) < 5e-9)
            {
                double t1 = data[i].time;
                ++i;
                //cout << 1e9*(data[i].time-t1) << endl;
                if(data[i].time-t1 > delt_min)
                {
                    TimeDist->Fill(1e9*(data[i].time-t1));
                    //                    cout << i << "\t"<< data[i].time-t1 << "\t" << data.size()<<endl;
                    testcount++;

                }
            }
            else
            {
                //cout << "should not happen"<< endl;
                ++i;
            }
        }
        data.clear();

    }
    cout << testcount << "\t" << nEntries << endl;
    // normalize histogram
    TimeDist->Sumw2();
    for(int iBin=1; iBin<=TimeDist->GetNbinsX(); iBin++)
    {
        TimeDist->SetBinContent(iBin,TimeDist->GetBinContent(iBin)/TimeDist->GetBinWidth(iBin));
        TimeDist->SetBinError(iBin,TimeDist->GetBinError(iBin)/TimeDist->GetBinWidth(iBin));
    }
    for(int iBin=1; iBin<=TimeDist->GetNbinsX(); iBin++)
    {
        TimeDist->SetBinContent(iBin,TimeDist->GetBinContent(iBin)/TimeDist->GetEntries());
        TimeDist->SetBinError(iBin,TimeDist->GetBinError(iBin)/TimeDist->GetEntries());
    }
    f.cd();
    TimeDist->Write();
    f.Close();






}

void DarkNtpAnalysis::alt_processTime()
{
     TFile f("tmp.root","RECREATE");
    f.cd();
    //Reads NTuple from .fanat# file in ntp/
    TFile* tFile = new TFile(mFileName);
    TNtuple* ntp = (TNtuple*) tFile->Get("ntp");
    int nEntries = ntp->GetEntries();
    //float ntpCont[20];
    float* ntpCont = ntp->GetArgs();
    char tHName[20];
    TH1D* tH = (TH1D*) gROOT->FindObjectAny("HDTime");
    if(tH) tH->Delete();
    int nBinMax=1001;
    double bin[1000];
    int nBin=0;
    double upperBound=pow(10,nBin/20.);
    while(upperBound<1e9 && nBin<nBinMax){
      bin[nBin]= upperBound;
      nBin++;
      upperBound = pow(10,nBin/20.);
    }
    bin[nBin]=upperBound;
    TH1D * HDTime = new TH1D("HDTime","HDTime",nBin,bin);





    int iEntry=0;
    while(iEntry<nEntries){
      // >>> Look for trigger
      do{
        ntp->GetEntry(iEntry);
        iEntry++;
      }while(iEntry<nEntries &&
             fabs(ntpCont[8]+0.02)>0.01 && //single PE bump
             fabs(ntpCont[9]-1e-9)<1e-9); // trigger pulse
      if(iEntry<nEntries){
        double tTriggerTime = ntpCont[1];
        double tTriggerPulseTime = ntpCont[9]+ntpCont[1];
        // >>> Check the next pulse
        ntp->GetEntry(iEntry);
        if(ntpCont[1]>=tTriggerTime){ // Do not account for the events at the sequence boundary
          do{
            ntp->GetEntry(iEntry);
            iEntry++;
          }while(iEntry<nEntries &&
                 ntpCont[8]>-0.01); // At least 1 PE
          if(iEntry<nEntries){
            HDTime->Fill((ntpCont[9]+ntpCont[1]-tTriggerPulseTime)*1e9);
          }
        }
      }
    }


    double scale = HDTime->GetEntries();
    HDTime->Sumw2();
    for(int iBin=1; iBin<=HDTime->GetNbinsX(); iBin++){
      HDTime->SetBinContent(iBin,HDTime->GetBinContent(iBin)/HDTime->GetBinWidth(iBin)/scale);
      HDTime->SetBinError(iBin,HDTime->GetBinError(iBin)/HDTime->GetBinWidth(iBin)/scale);
    }
    TCanvas *c = new TCanvas("c","c",1600,900);
    c->SetLogy(1);
    c->SetLogx(1);

    HDTime->Draw("");
    c->SaveAs("test.pdf");














//    TNtuple* ntp = (TNtuple*) tFile->Get("ntp");
//    NtpCont* cont = (NtpCont*) ntp->GetArgs();
//    float* ntpCont = ntp->GetArgs();
//    int nBinMax=1001;
//    double bin[1000];
//    int nBin=0;
//    double upperBound=pow(10,nBin/20.);
//    while(upperBound<1e9 && nBin<nBinMax){
//      bin[nBin]= upperBound;
//      nBin++;
//      upperBound = pow(10,nBin/20.);
//    }
//    bin[nBin]=upperBound;
//    alt_TimeDist = new TH1D("alt_TimeDist","alt_TimeDist",nBin,bin);  //Creates timing distribution histogram
//    TimeDist = new TH1D("TimeDist","TimeDist",nBin,bin);
//    TH2D ptpa("ptpa","ptpa",100,-50e-9,1500e-9,100,-0.005,0.08);
//    ptpa.SetMarkerStyle(2);
//    int nEntries = ntp->GetEntries();


//    int iEntry=0;
//    while(iEntry<nEntries){
//      // >>> Look for trigger
//      do{
//        ntp->GetEntry(iEntry);
//        iEntry++;
//      }while(iEntry<nEntries &&
//             fabs(ntpCont[8]+0.02)>0.01 && //single PE bump
//             fabs(ntpCont[9]-1e-9)<1e-9); // trigger pulse
//      if(iEntry<nEntries){
//        double tTriggerTime = ntpCont[1];
//        double tTriggerPulseTime = ntpCont[9]+ntpCont[1];
//        // >>> Check the next pulse
//        ntp->GetEntry(iEntry);
//        if(ntpCont[1]>=tTriggerTime){ // Do not account for the events at the sequence boundary
//          do{
//            ntp->GetEntry(iEntry);
//            iEntry++;
//          }while(iEntry<nEntries &&
//                 ntpCont[8]>-0.01); // At least 1 PE
//          if(iEntry<nEntries){
//            alt_TimeDist->Fill((ntpCont[9]+ntpCont[1]-tTriggerPulseTime)*1e9);
//          }
//        }
//      }
//    }

//    /*    ntp->GetEntry(counter);
//    while(counter < nEntries)
//    {
//        pulse _evt;
//        while(tPrev <= cont->tt && counter < nEntries) // add "&& event == cont->evt" in case you only want one waveform!
//        {
//            tPrev = cont->tt;
//            _evt.time = cont->ft + cont->tt;
//            _evt.uppeak = cont->nl-cont->blmu;
//            _evt.ft = cont->ft;
//            _evt.ampl = cont->pa;
//            _evt.chi2 = cont->frchi2/cont->frndf;
//            _evt.pt = cont->tt + cont->pt;
//            _evt.tpt = cont->pt;
//            _evt.pw = cont->pw;
//            data.push_back(_evt);
//            if(_evt.pw>6)
//                ptpa.Fill(cont->pt, _evt.ampl);
//            counter++;

//            ntp->GetEntry(counter);
//            //cout << _evt.pw << endl;
//        }
//        tPrev =-1;
//        // sort the data - should not make any difference, but just for safety
//        sort(data.begin(), data.end(), comp);
//        // Fill the histo with the time differences
//        int i = 0;
//        while(i<(data.size()-1))
//        {
//            // mother pulse should be a SPE and at the trigger time to keep the dead window sharp
//            //if(data[i].ampl < speupper && data[i].ampl > spelower && data[i].uppeak<0.005 && abs(data[i].ft) < 5e-9 && data[i].uppeak < 0.0075) //&& data[i].chi2==1)
//            if(data[i].ampl>spelower && data[i].ampl<speupper && data[i].pw>6 && abs(data[i].tpt)<1)
//            {
//                double t1 = data[i].pt;
//                ++i;
//                // search for the next pulse with the required amplitude and time dist in sequence
//                while((data[i].pt-t1 < delt_min || data[i].ampl<spelower) && i < data.size()-1)
//                {
//                    ++i;
//                    //if(data[i].pt-t1<0)
//                    bad++;
//                }
//                // if found fill it in
//                //if(data[i].time-t1 > delt_min  && data[i].ampl<-0.005 && data[i].uppeak < 0.0075) //&& data[i].chi2==1)
//                if((data[i].pt-t1 > delt_min && data[i].ampl > spelower))
//                {
//                    alt_TimeDist->Fill(1e9*(data[i].pt-t1));
//                    Good +=(1e9*(data[i].pt-t1));
//                    testcount++;
//                    good++;

//                }
//            }
//            else
//            {
//                //cout << "should not happen"<< endl;
//                ++i;
//            }
//        }
//        data.clear();

//    }*/
//    double BinContents = alt_TimeDist->GetEntries();
//    alt_TimeDist->Sumw2();
//    for(int iBin=1; iBin<=alt_TimeDist->GetNbinsX(); iBin++)
//    {
//        alt_TimeDist->SetBinContent(iBin,alt_TimeDist->GetBinContent(iBin)/alt_TimeDist->GetBinWidth(iBin)/BinContents);
//        alt_TimeDist->SetBinError(iBin,alt_TimeDist->GetBinError(iBin)/alt_TimeDist->GetBinWidth(iBin)/BinContents);
//    }
//    f.cd();
//    alt_TimeDist->SetTitle("Timing Distribution");
//    alt_TimeDist->GetXaxis()->SetTitle("Time [ns]");
//    alt_TimeDist->GetYaxis()->SetTitle("Probability Density [ns^{-1}]");
//    alt_TimeDist->Write();
//    f.Write();
//    f.Close();

//    TCanvas *c = new TCanvas("c","c",1600,900);

//    // Fit the recovery
//    //c->SetLogz();

//    c->SaveAs("2d.png");
//    c->Clear();
//    c->SetLogy();
//    c->SetLogx();
//    // fit a exponential for normalization
//    TF1* expo = new TF1("expo", funcDN, 5e6, 8e8,2);
//    expo->SetParameter(0,1./1e8);
//    expo->SetParameter(1,1e-9);
//    alt_TimeDist->Draw("");
//    cout << "Expo Fit" << endl;
//    c->SaveAs("test.pdf");
//    alt_TimeDist->Fit("expo","R");
//    gStyle->SetOptStat(1);


//    expo->SetRange(10,1e9);
//    expo->SetLineWidth(0.5);
//    expo->Draw("same");
//    c->Clear();



//    // second stage fit the long timeconstant
//    TF1 *finalFit= new TF1("finalFit", multiple_tc,5e4,8e9, 4);
//    finalFit->FixParameter(0,1./expo->GetParameter(0));
//    finalFit->FixParameter(1,1);
//    finalFit->SetParameter(2,9e5);
//    finalFit->SetParameter(3,0.06);
//    cout << "DNAP1 - Lennart" << endl;
//    alt_TimeDist->Fit(finalFit,"R");
//    TF1 *finalFit_alt= new TF1("finalFit_alt", funcDNAP1,5e4,8e9, 3);
//    finalFit_alt->FixParameter(0,expo->GetParameter(0));
//    finalFit_alt->SetParameter(1,0.06);
//    finalFit_alt->SetParameter(2,9e5);
//    cout << "DNAP1-Fabrice" << endl;
//    alt_TimeDist->Fit(finalFit_alt,"R");
//    finalFit_alt->SetRange(10,1e9);
//    alt_TimeDist->Draw("");
//    finalFit_alt->Draw("same");
//    //expo->Draw("same");
//    c->SaveAs("test2.pdf");
//    c->Clear();

//    // third stage: fit the short time constants:
//    TF1 *lastFit = new TF1("lastfit", multiple_tc,10,1e3,6);
//    lastFit->FixParameter(0,finalFit->GetParameter(0));
//    lastFit->FixParameter(1,2);
//    lastFit->FixParameter(2,finalFit->GetParameter(2));
//    lastFit->FixParameter(3,finalFit->GetParameter(3));
//    lastFit->SetParameter(4,20);
//    lastFit->SetParameter(5,0.05);

//    alt_TimeDist->Fit(lastFit,"R");
//    lastFit->SetRange(10,9e3);

//    c->Clear();
//    alt_TimeDist->Draw("");
//    lastFit->Draw("same");

//    lastFit->SetLineWidth(0.1);
//    c->SaveAs("test3.pdf");
//    cout << lastFit->GetChisquare()/lastFit->GetNDF() << endl;
//    cout << lastFit->Integral(10,1e6) << endl;
}

void DarkNtpAnalysis::ProcessDTime(bool aSaveGraph)
{
    //Reads NTuple from .fanat# file in ntp/
    TFile* tFile = new TFile(mFileName);//mFileName
    TNtuple* ntp = (TNtuple*) tFile->Get("ntp");
    NtpCont* cont = (NtpCont*) ntp->GetArgs();

    int nBinMax=1000;
    double bin[1000];
    double darkNoiseBinWidth=100.;//ns
    double lowestBound=-900.;
    double upperBound=0.;
    int iBin=0;

    //Sets logarithmic lower bin limits
    while(upperBound<1.0e6 && iBin<nBinMax)
    {
        bin[iBin]=upperBound;
        upperBound = pow(10,iBin/20.);
        iBin++;
    }

    bin[iBin]=upperBound;
    TimeDist = new TH1D("TimeDist","TimeDist",iBin,bin);  //Creates timing distribution histogram

    int nEntry = ntp->GetEntries();

    int iFirstEntry=0;
    ntp->GetEntry(iFirstEntry);
    double tTime = (cont->ft+cont->tt)*1e9;
    double tPrev = tTime;
    double fTime;
    int counter = iFirstEntry + 1;
    double times[10];
    while (counter < nEntry)
    {
        ntp->GetEntry(counter);
        times[0] = cont->ft;
        if(cont->np > 1)
        {
            //Fills fitted times of pulses into an array
            for(int i = 1; i < cont->np; i++)
            {
                counter++;
                ntp->GetEntry(counter);
                times[i] = cont->ft;
            }
        }
        /*Reads backwards through the array (for some reason the ntuple is filled in reverse order
            for each event) and fills the histogram with the time between each peak and its predecessor.
            The time of each peak is the summer of the trigger time and the fitted time*/
        for(int i = cont->np - 1; i >= 0; i--)
        {
            tTime =  (times[i]+cont->tt)*1e9;
            if(tTime-tPrev > 10)
                TimeDist->Fill(tTime-tPrev);
            tPrev=tTime;
        }
        counter++;
    }

    //Corrects bin values to account for logarithmic binning, and normalises to 1
    double scale = TimeDist->Integral("width");
    TimeDist->Sumw2();
    for(int iBin=1; iBin<=TimeDist->GetNbinsX(); iBin++)
    {
        TimeDist->SetBinContent(iBin,TimeDist->GetBinContent(iBin)/TimeDist->GetBinWidth(iBin)/scale);
        TimeDist->SetBinError(iBin,TimeDist->GetBinError(iBin)/TimeDist->GetBinWidth(iBin)/scale);
    }

    double DNPercErr;
    double APulsePercErr;

    //Fits DN-dominated section of timing distribution with an exponential, used to fix the DN rate constant when fitting whole distribution.
    TF1* DTFit1 = new TF1("DTFit1", "expo", 1e3, 1e6);
    TimeDist->Fit("DTFit1");
    DNPercErr = abs(DTFit1->GetParError(1) / DTFit1->GetParameter(1));

    //Fits the distribution after 6ns with the fitting function
    TF1* DTFit2 = new TF1("DTFit2", fitFunction, 6, 1e6, 4);
    DTFit2->FixParameter(1, -1/DTFit1->GetParameter(1));
    DTFit2->SetParameter(0, 1e-3);
    DTFit2->SetParameter(3, 300);
    DTFit2->SetParameter(2, 1e-5);
    TimeDist->Fit("DTFit2", "R");
    APulsePercErr = abs(DTFit2->GetParError(3) / DTFit2->GetParameter(3));

    //Extends the fitted curve through the first 6ns
    DTFit2->SetRange(1, 1e6);
    DTFit2->FixParameter(0, DTFit2->GetParameter(0));
    DTFit2->FixParameter(2, DTFit2->GetParameter(2));
    DTFit2->FixParameter(3, DTFit2->GetParameter(3));
    TimeDist->Fit("DTFit2", "R");

    //Calculates DN rate and Apulsing rate and their errors
    DNRate = (1/DTFit2->GetParameter(1)) * 1e9;
    DNRate_E = DNPercErr * DNRate;
    APulseRate = (1/DTFit2->GetParameter(3)) * 1e9;
    APulseRate_E = APulsePercErr * APulseRate;

    //Optionally saves histogram to file
    if(aSaveGraph)
    {
        TCanvas *c1 = new TCanvas("Timing Distribution", "Timing Distribution", 900, 600);
        c1->SetLogy();
        c1->SetLogx();
        gStyle->SetOptStat(0);
        TimeDist->SetTitle("Timing Distribution");
        TimeDist->GetXaxis()->SetTitle("Time (ns)");
        TimeDist->GetYaxis()->SetTitle("Probability Density (ns^{-1})");
        TimeDist->Draw();
        DTFit2->Draw("same");
        char savename[100];
        sprintf(savename, "%s/Run%i-TimeDist.pdf", mSavePath, mRun);
        c1->SaveAs(savename);
    }
}

void DarkNtpAnalysis::ProcessXTalk(bool aSaveGraph, double one_pe_upperthresh, double two_pe_lowerthresh)
{

    //Imports ntuple data from datafile
    TFile* tFile = new TFile(mFileName);
    TNtuple* ntp = (TNtuple*) tFile->Get("ntp");
    NtpCont* cont = (NtpCont*) ntp->GetArgs();

    XTalkDist = new TH1D("XTalkDist", "XTalkDist", 60, -0.1, 0);

    //Fills histogram with peak fitted amplitudes
    for(int i = 0; i<ntp->GetEntries(); ++i)
    {
        ntp->GetEntry(i);
        XTalkDist->Fill(cont->fa);
    }

    //Estimates a point that lies between the 1pe and 2pe peaks
    double interpeak_thresh = (one_pe_upperthresh + two_pe_lowerthresh) / 2;

    //Finds starting values for the bins containing the 2 peaks
    int one_pe_peak = XTalkDist->FindBin(interpeak_thresh);
    int two_pe_peak = XTalkDist->FindBin(two_pe_lowerthresh);

    int interpeak_min; //The minimum between 2pe and 1pe
    int prepeak_min; //The minimum between 1pe and 0pe

    //Finds the highest point between the bounds set on the location of the 2pe peak
    for(int i = XTalkDist->FindBin(two_pe_lowerthresh); i < XTalkDist->FindBin(interpeak_thresh); i++)
    {
        if(XTalkDist->GetBinContent(i) > XTalkDist->GetBinContent(two_pe_peak))
        {
            two_pe_peak = i;
        }
    }

    //Finds the highest point between the bounds set on the location of the 1pe peak
    for(int i = XTalkDist->FindBin(interpeak_thresh); i < XTalkDist->FindBin(one_pe_upperthresh); i++)
    {
        if(XTalkDist->GetBinContent(i) > XTalkDist->GetBinContent(one_pe_peak))
        {
            one_pe_peak = i;
        }
    }

    //Finds the lowest value between the 2 peaks, and thus the x-talk thresh
    interpeak_min = two_pe_peak;
    for(int i = two_pe_peak; i < one_pe_peak; i++)
    {
        if(XTalkDist->GetBinContent(i) < XTalkDist->GetBinContent(interpeak_min))
        {
            interpeak_min = i;
        }
    }
    XTalkThresh = XTalkDist->GetBinCenter(interpeak_min);
    XTalkThresh_E = 0.5 * XTalkDist->GetBinWidth(interpeak_min);

    //Finds the lowest point between the 1pe peak and 0
    prepeak_min = one_pe_peak;
    for(int i = one_pe_peak; i < XTalkDist->FindBin(0.0); i++)
    {
        if(XTalkDist->GetBinContent(i) < XTalkDist->GetBinContent(prepeak_min))
        {
            prepeak_min = i;
        }
    }

    /*Calculates the X-Talk probability by taking the ratio of the integral of the 1pe peak
        to the integral of all successive peaks*/
    double error1;
    double error2;
    double integral1 = XTalkDist->IntegralAndError(0, interpeak_min, error1, "width");
    double integral2 = XTalkDist->IntegralAndError(0, prepeak_min, error2, "width");
    XTalkProb = integral1 / integral2;
    XTalkProb_E = XTalkProb * (error1 / integral1 + error2 / integral2);

    //Optionally saves the histogram to file, with the found maxima and minima superimposed.
    if(aSaveGraph)
    {
        TGraph* minima = new TGraph();
        minima->SetPoint(0, XTalkDist->GetBinCenter(interpeak_min), XTalkDist->GetBinContent(interpeak_min));
        minima->SetPoint(1, XTalkDist->GetBinCenter(prepeak_min), XTalkDist->GetBinContent(prepeak_min));
        minima->SetMarkerStyle(23);
        minima->SetMarkerColor(4);
        minima->SetMarkerSize(2);

        TGraph* maxima = new TGraph();
        maxima->SetPoint(0, XTalkDist->GetBinCenter(one_pe_peak), XTalkDist->GetBinContent(one_pe_peak));
        maxima->SetPoint(1, XTalkDist->GetBinCenter(two_pe_peak), XTalkDist->GetBinContent(two_pe_peak));
        maxima->SetMarkerStyle(22);
        maxima->SetMarkerColor(2);
        maxima->SetMarkerSize(2);

        TCanvas* c1 = new TCanvas("XTalkDist", "XTalkDist", 1200, 900);
        c1->SetLogy();
        XTalkDist->SetTitle("Cross-Talk Distribution");
        XTalkDist->GetYaxis()->SetTitle("Counts");
        XTalkDist->GetXaxis()->SetTitle("Peak Amplitude (V)");
        XTalkDist->Draw();
        minima->Draw("same P");
        maxima->Draw("same P");

        char savename[100];
        sprintf(savename, "%s/Run%i-XTalk.pdf", mXTalkSavePath, mRun);
        c1->SaveAs(savename);
    }
}

void DarkNtpAnalysis::WriteStats()
{
    //Writes the calculated statistics to a text file.
    cout << "Filename: " << mFileName << " SavePath: " << mSavePath << endl;
    ofstream oFile;
    char savename[100];
    sprintf(savename, "%s/Run%i-stats.txt", mSavePath, mRun);
    oFile.open(savename);
    oFile << "Dark Noise Rate (Hz)\tAfterpulsing Rate (Hz)\tCross-Talk Probability\tCross-Talk Threshold (V)\n";
    oFile << DNRate << " ± " << DNRate_E << "\t" << APulseRate << " ± " << APulseRate_E << "\t" << XTalkProb << " ± " << XTalkProb_E << "\t" << XTalkThresh << " ± " << XTalkThresh_E;
    oFile.close();
    cout << "Statistics written in " << savename << endl;
}

char* DarkNtpAnalysis::getFileName()
{
    //Iterates through RunInfo.txt to find the specified run number and matching file path
    char FileNames[2][100];
    char* activename = new char[1024];
    char* outFileName = new char[1024];

    ifstream iFile;
    iFile.open(mRunInfo);
    char buffer [100];
    int runnum;
    char* newbuff;
    for (int i = 0; i < 11; i++){
        iFile >> buffer;
    }
    while(!iFile.eof()){
        iFile >> FileNames[0];
        iFile >> FileNames[1];
        sscanf(FileNames[0], "%d", &runnum);
        if((runnum == mRun)){
            newbuff = FileNames[1];
            sprintf(activename, "%s", newbuff);
        }
        for (int i = 0; i < 9; i++){
            iFile >> buffer;
        }
    }

    //Strips the path from the file name, prefixes the file name with ntp/, and suffixes it with .fanat#
    iFile.close();
    int slashIndex=0;
    for(int index=0; index<strlen(activename); index++){
        if(strncmp(activename+index,"/",1)==0) slashIndex=index;
    }
    sprintf(outFileName,"ntp%s.fanat%i",activename+slashIndex,mFitType);
    return outFileName;
}
