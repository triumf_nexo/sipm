#include <sys/stat.h>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <fstream>

#include "TFile.h"
#include "TNtuple.h"
#include "TF1.h"
#include "TROOT.h"
#include "TH1D.h"
#include "TGraph.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TSystem.h"
#include "TStyle.h"

// set the values of the blind window and the cut for the pulsefinder
const double gapUpperBound = 12.e3,
gapLowerBound = 9.5e3;
double GapStart = gapLowerBound,
GapEnd = gapUpperBound,
Starttime = 10.;
double DeadTime = 1.;//ns

double funcDN(double* x, double* p){
    if(x[0]<DeadTime) return 0.;
    if(x[0]<GapStart){
        return p[0]*(1.-p[1])*exp(-p[0]*(x[0]-DeadTime));
    }
    else{
        if(x[0]<GapEnd) return 0.;
        return p[0]*(1.-p[1])*exp(-p[0]*(x[0]-DeadTime-GapEnd+GapStart));
    }
}
double funcDNAP2WGapRec(double* x, double* p){
    // p[0] = dark noise rate
    // p[1] = <nap1>
    // p[2] = tauAp1
    // p[3] = <nap2>
    // p[4] = tauAp2
    // p[5] = tauRec
    static int postGapCalc;
    static double noAP1PostGapCorr;
    static double noAP2PostGapCorr;
    if(x[0]<DeadTime) return 0.;
    if(x[0]<GapStart || x[0]>GapEnd){

        double noAP1=1.;
        double singleAP1=0.;
        double Poisson;
        double expTau1 = exp(-x[0]/p[2]);
        double expDeadTime1 = exp(-DeadTime/p[2]);
        double tauRecEff1 = p[2]*p[5]/(p[2]+p[5]);
        double expTauRec1 = exp(-x[0]/tauRecEff1);
        double expDeadTimeRec1 = exp(-DeadTime/tauRecEff1);
        double factPar1 = 1.;
        double noAP2=1.;
        double singleAP2=0.;
        double expTau2 = exp(-x[0]/p[4]);
        double expDeadTime2 = exp(-DeadTime/p[4]);
        double tauRecEff2 = p[4]*p[5]/(p[4]+p[5]);
        double expTauRec2 = exp(-x[0]/tauRecEff2);
        double expDeadTimeRec2 = exp(-DeadTime/tauRecEff2);
        double factPar2 = 1.;
        for(int iAP=1; iAP<5; iAP++) {
            Poisson=TMath::Poisson(iAP,p[1]);
            singleAP1 += Poisson*(iAP==1? 1. : (1.-factPar1));
            factPar1*=(expDeadTime1-expTau1-
                       tauRecEff1/p[2]*(expDeadTimeRec1-expTauRec1));
            noAP1 -= Poisson*factPar1;
            Poisson=TMath::Poisson(iAP,p[3]);
            singleAP2 += Poisson*(iAP==1? 1. : (1.-factPar2));
            factPar2*=(expDeadTime2-expTau2-
                       tauRecEff2/p[4]*(expDeadTimeRec2-expTauRec2));
            noAP2 -= Poisson*factPar2;
        }
        singleAP1*=expTau1/p[2]*(1.-exp(-x[0]/p[5]));
        singleAP2*=expTau2/p[4]*(1.-exp(-x[0]/p[5]));
        if(x[0]<GapStart){
            postGapCalc=0;
            return exp(-p[0]*(x[0]-DeadTime))*(noAP1*noAP2*p[0]+noAP2*singleAP1+noAP1*singleAP2);
        }
        else{ //Subrtract out the
            if(postGapCalc==0){
                postGapCalc=1;
                double Poisson;
                noAP1PostGapCorr=0.; // probability that AP1 is in the gap
                double expTauStart1 = exp(-GapStart/p[2]);
                double expTauEnd1 = exp(-GapEnd/p[2]);
                double expDeadTime1 = exp(-DeadTime/p[2]);
                double tauRecEff1 = p[2]*p[5]/(p[2]+p[5]);
                double expTauRecStart1 = exp(-GapStart/tauRecEff1);
                double expTauRecEnd1 = exp(-GapEnd/tauRecEff1);
                double expDeadTimeRec1 = exp(-DeadTime/tauRecEff1);
                double factPar1 = 1.;
                noAP2PostGapCorr=0.;
                double expTauStart2 = exp(-GapStart/p[4]);
                double expTauEnd2 = exp(-GapEnd/p[4]);
                double expDeadTime2 = exp(-DeadTime/p[4]);
                double tauRecEff2 = p[4]*p[5]/(p[4]+p[5]);
                double expTauRecStart2 = exp(-GapStart/tauRecEff2);
                double expTauRecEnd2 = exp(-GapEnd/tauRecEff2);
                double expDeadTimeRec2 = exp(-DeadTime/tauRecEff2);
                double factPar2 = 1.;
                for(int iAP=1; iAP<5; iAP++) {
                    Poisson=TMath::Poisson(iAP,p[1]);
                    factPar1*=(expTauStart1-expTauEnd1-
                               tauRecEff1/p[2]*(expTauRecStart1-expTauRecEnd1));
                    noAP1PostGapCorr += Poisson*factPar1;
                    Poisson=TMath::Poisson(iAP,p[3]);
                    factPar2*=(expTauStart2-expTauEnd2-
                               tauRecEff2/p[4]*(expTauRecStart2-expTauRecEnd2));
                    noAP2PostGapCorr += Poisson*factPar2;
                }
                cout << x[0] << " " << noAP1 << " " << noAP1PostGapCorr << " "
                             << noAP2 << " " << noAP2PostGapCorr << std::endl;
            }
            noAP1+=noAP1PostGapCorr;
            noAP2+=noAP2PostGapCorr;
            return exp(-p[0]*(x[0]-DeadTime-GapEnd+GapStart)*(noAP1*noAP2*p[0]+noAP2*singleAP1+noAP1*singleAP2));
        }
    }
    else{
        return 0.;
    }
}


double funcDNAP2WGapRecSimple(double* x, double* p){
    // p[0] = dark noise rate
    // p[1] = <nap1>
    // p[2] = tauAp1
    // p[3] = <nap2>
    // p[4] = tauAp2
    // p[5] = tauRec
    //static int postGapCalc;
    //static double noAP1PostGapCorr;
    //static double noAP2PostGapCorr;
    if(x[0]<DeadTime) return 0.;
    if(x[0]<GapStart || x[0]>GapEnd){
        double singleAP1 =p[1]/p[2]*exp(-x[0]/p[2])*(1.-exp(-x[0]/p[5]));
        double tauRecEff1 = p[2]*p[5]/(p[2]+p[5]);
        double noAP1 = 1.- p[1]*(exp(-DeadTime/p[2])-exp(-x[0]/p[2])-tauRecEff1/p[2]*(exp(-DeadTime/tauRecEff1)-exp(-x[0]/tauRecEff1)));
        double singleAP2 =p[3]/p[4]*exp(-x[0]/p[4])*(1.-exp(-x[0]/p[5]));
        double tauRecEff2 = p[4]*p[5]/(p[4]+p[5]);
        // changed exp(-x[0]/p[2]) to exp(-x[0]/p[4])
        double noAP2 = 1.- p[3]*(exp(-DeadTime/p[4])-exp(-x[0]/p[4])-tauRecEff2/p[4]*(exp(-DeadTime/tauRecEff2)-exp(-x[0]/tauRecEff2)));
        if(x[0]<GapStart){
            //postGapCalc=-1;
            return exp(-p[0]*(x[0]-DeadTime))*(noAP1*noAP2*p[0]+noAP2*singleAP1+noAP1*singleAP2);
        }
        else{ //Subtract out the
            //if(postGapCalc<=0){
            //postGapCalc=1;
            double noAP1PostGapCorr= p[1]*(exp(-GapStart/p[2])-exp(-GapEnd/p[2])-exp(-x[0]/p[2])-tauRecEff1/p[2]*(exp(-GapStart/tauRecEff1)-exp(-GapEnd/tauRecEff1)));
            double noAP2PostGapCorr= p[3]*(exp(-GapStart/p[4])-exp(-GapEnd/p[4])-tauRecEff2/p[4]*(exp(-GapStart/tauRecEff2)-exp(-GapEnd/tauRecEff2)));
            //cout << x[0] << " "
            //   << singleAP1 << " " << noAP1 << " " << noAP1PostGapCorr << " "
            //   << singleAP2 << " " << noAP2 << " " << noAP2PostGapCorr << endl;
            //}
            noAP1+=noAP1PostGapCorr;
            noAP2+=noAP2PostGapCorr;
            return exp(-p[0]*(x[0]-DeadTime-GapEnd+GapStart))*(noAP1*noAP2*p[0]+noAP2*singleAP1+noAP1*singleAP2);
        }
    }
    else{
        return 0.;
    }
}


double funcDNAP3WGapRecSimple(double* x, double* p){
    // p[0] = dark noise rate
    // p[1] = <nap1>
    // p[2] = tauAp1
    // p[3] = <nap2>
    // p[4] = tauAp2
    // p[5] = tauRec
    // p[6] = <nap2>
    // p[7] = tauAp3
    //static int postGapCalc;
    //static double noAP1PostGapCorr;
    //static double noAP2PostGapCorr;
    if(x[0]<DeadTime) return 0.;
    if(x[0]<GapStart || x[0]>GapEnd){
        double singleAP1 =p[1]/p[2]*exp(-x[0]/p[2])*(1.-exp(-x[0]/p[5]));
        double tauRecEff1 = p[2]*p[5]/(p[2]+p[5]);
        double noAP1 = 1.- p[1]*(exp(-DeadTime/p[2])-exp(-x[0]/p[2])-tauRecEff1/p[2]*(exp(-DeadTime/tauRecEff1)-exp(-x[0]/tauRecEff1)));

        double singleAP2 =p[3]/p[4]*exp(-x[0]/p[4])*(1.-exp(-x[0]/p[5]));
        double tauRecEff2 = p[4]*p[5]/(p[4]+p[5]);
        double noAP2 = 1.- p[3]*(exp(-DeadTime/p[4])-exp(-x[0]/p[4])-tauRecEff2/p[4]*(exp(-DeadTime/tauRecEff2)-exp(-x[0]/tauRecEff2)));

        double singleAP3 =p[6]/p[7]*exp(-x[0]/p[7])*(1.-exp(-x[0]/p[7]));
        double tauRecEff3 = p[7]*p[5]/(p[7]+p[5]);
        double noAP3 = 1.- p[6]*(exp(-DeadTime/p[7])-exp(-x[0]/p[6])-tauRecEff3/p[7]*(exp(-DeadTime/tauRecEff3)-exp(-x[0]/tauRecEff3)));


        if(x[0]<GapStart){
            //postGapCalc=-1;
            return exp(-p[0]*(x[0]-DeadTime))*(noAP1*noAP2*noAP3*p[0]+noAP2*noAP3*singleAP1+noAP1*noAP3*singleAP2+ noAP1 * noAP2* singleAP3);
        }
        else{ //Subtract out the
            //if(postGapCalc<=0){
            //postGapCalc=1;
            double noAP1PostGapCorr= p[1]*(exp(-GapStart/p[2])-exp(-GapEnd/p[2])-exp(-x[0]/p[2])
                    -tauRecEff1/p[2]*(exp(-GapStart/tauRecEff1)-exp(-GapEnd/tauRecEff1)));
            double noAP2PostGapCorr= p[3]*(exp(-GapStart/p[4])-exp(-GapEnd/p[4])-exp(-x[0]/p[4])-
                    tauRecEff2/p[4]*(exp(-GapStart/tauRecEff2)-exp(-GapEnd/tauRecEff2)));
            //cout << x[0] << " "
            //   << singleAP1 << " " << noAP1 << " " << noAP1PostGapCorr << " "
            //   << singleAP2 << " " << noAP2 << " " << noAP2PostGapCorr << endl;
            //}
            noAP1+=noAP1PostGapCorr;
            noAP2+=noAP2PostGapCorr;
            return exp(-p[0]*(x[0]-DeadTime-GapEnd+GapStart))*(noAP1*noAP2*p[0]+noAP2*singleAP1+
                    noAP1*singleAP2);
        }
    }
    else{
        return 0.;
    }
}
double funcDNAP2W2WOGapRecSimple(double* x, double* p){
    // p[0] = dark noise rate
    // p[1] = <nap1>
    // p[2] = tauAp1
    // p[3] = <nap2>
    // p[4] = tauAp2
    // p[5] = tauRec
    // p[6] = <nap3>
    // p[7] = tauAp3
    // p[8] = <nap4>
    // p[9] = tauAp4
    //static int postGapCalc;
    //static double noAP1PostGapCorr;
    //static double noAP2PostGapCorr;
    if(x[0]<DeadTime) return 0.;
    if(x[0]<GapStart || x[0]>GapEnd){
        double singleAP1 =p[1]/p[2]*exp(-x[0]/p[2])*(1.-exp(-x[0]/p[5]));
        double tauRecEff1 = p[2]*p[5]/(p[2]+p[5]);
        double noAP1 = 1.- p[1]*(exp(-DeadTime/p[2])-exp(-x[0]/p[2])-tauRecEff1/p[2]*(exp(-DeadTime/tauRecEff1)-exp(-x[0]/tauRecEff1)));

        double singleAP2 =p[3]/p[4]*exp(-x[0]/p[4])*(1.-exp(-x[0]/p[5]));
        double tauRecEff2 = p[2]*p[5]/(p[2]+p[5]);
        double noAP2 = 1.- p[3]*(exp(-DeadTime/p[4])-exp(-x[0]/p[4]) -tauRecEff2/p[4]*(exp(-DeadTime/tauRecEff2)-exp(-x[0]/tauRecEff2)));

        double singleAP3 =p[6]/p[7]*exp(-x[0]/p[7])*(1.-exp(-x[0]/p[7]));
        double tauRecEff3 = p[7]*p[5]/(p[7]+p[5]);
        double noAP3 = 1.- p[6]*(exp(-DeadTime/p[7])-exp(-x[0]/p[6]) -tauRecEff3/p[7]*(exp(-DeadTime/tauRecEff3)-exp(-x[0]/tauRecEff3)));

        double singleAP4 =p[8]/p[9]*exp(-x[0]/p[9])*(1.-exp(-x[0]/p[9]));
        double tauRecEff4 = p[9]*p[5]/(p[9]+p[5]);
        double noAP4 = 1.- p[8]*(exp(-DeadTime/p[9])-exp(-x[0]/p[8]) -tauRecEff4/p[9]*(exp(-DeadTime/tauRecEff4)-exp(-x[0]/tauRecEff4)));


        if(x[0]<GapStart){
            //postGapCalc=-1;
            return exp(-p[0]*(x[0]-DeadTime))*(noAP1*noAP2*noAP3*noAP4*p[0]+
                    noAP2*noAP3*noAP4*singleAP1+
                    noAP1*noAP3*noAP4*singleAP2+
                    noAP1 * noAP2* noAP4*singleAP3+
                    noAP1 * noAP2* noAP3*singleAP4
                    );
        }
        else{ //Subtract out the
            //if(postGapCalc<=0){
            //postGapCalc=1;
            double noAP1PostGapCorr= p[1]*(exp(-GapStart/p[2])-exp(-GapEnd/p[2])-exp(-x[0]/p[2])-tauRecEff1/p[2]*(exp(-GapStart/tauRecEff1)-exp(-GapEnd/tauRecEff1)));
            double noAP2PostGapCorr= p[3]*(exp(-GapStart/p[4])-exp(-GapEnd/p[4])-tauRecEff2/p[4]*(exp(-GapStart/tauRecEff2)-exp(-GapEnd/tauRecEff2)));
            //cout << x[0] << " "
            //   << singleAP1 << " " << noAP1 << " " << noAP1PostGapCorr << " "
            //   << singleAP2 << " " << noAP2 << " " << noAP2PostGapCorr << endl;
            //}
            noAP1+=noAP1PostGapCorr;
            noAP2+=noAP2PostGapCorr;
            return exp(-p[0]*(x[0]-DeadTime-GapEnd+GapStart))*(noAP1*noAP2*noAP3*p[0]+noAP2*noAP3*singleAP1
                    +noAP1*noAP3*singleAP2);
        }
    }
    else{
        return 0.;
    }
}

// just plotting helpers


void PFTime(const char* aFileName)
{
    TFile* tFile = (TFile*) gROOT->GetListOfFiles()->FindObject(aFileName);
    if(!tFile) tFile = new TFile(aFileName);

    //Reads NTuple from .fanat# file in ntp/
    TNtuple* ntp = (TNtuple*) tFile->Get("ntp");
    float* ntpCont = ntp->GetArgs();
    int nBinMax=1001;
    double bin[1000];
    int nBin=0;
    double upperBound=pow(10,nBin/20.);
    while(upperBound<1000e9 && nBin<nBinMax){
        bin[nBin]= upperBound;
        nBin++;
        upperBound = pow(10,nBin/20.);
    }
    bin[nBin]=upperBound;
    TH1D * alt_TimeDist = new TH1D("alt_TimeDist","alt_TimeDist",nBin,bin);  //Creates timing distribution histogram
    int nEntries = ntp->GetEntries();
    double speupper =  0.045,//-0.03,//
            spelower = 0.03; //-0.05;//
    double delt_min = 1e-9;
    int bad = 0, good = 0;
    // read in the ntuple
    int counter = 0;
    double tPrev=-1;
    int testcount = 0;
    double Good = 0;

    int iEntry=0;
    while(iEntry<nEntries){
        // >>> Look for trigger
        do{
            ntp->GetEntry(iEntry);
            iEntry++;
        }while(iEntry<nEntries &&
               abs(ntpCont[5]+0.0375)<0.0075 && //single PE bump
               abs(ntpCont[6])>15e-9); // trigger pulse
        if(iEntry<nEntries){
            double tTriggerTime = ntpCont[1];
            double tTriggerPulseTime = ntpCont[6]+ntpCont[1];
            // >>> Check the next pulse
            ntp->GetEntry(iEntry);
            if(ntpCont[1]>=tTriggerTime){ // Do not account for the events at the sequence boundary
                do{
                    ntp->GetEntry(iEntry);
                    iEntry++;
                }while(iEntry<nEntries &&
                       ntpCont[5]<0.03); // At least 1 PE
                if(iEntry<nEntries){
                    alt_TimeDist->Fill((ntpCont[6]+ntpCont[1]-tTriggerPulseTime)*1e9);
                }
            }
        }
    }


    double BinContents = alt_TimeDist->GetEntries();
    alt_TimeDist->Sumw2();
    for(int iBin=1; iBin<=alt_TimeDist->GetNbinsX(); iBin++)
    {
        alt_TimeDist->SetBinContent(iBin,alt_TimeDist->GetBinContent(iBin)/alt_TimeDist->GetBinWidth(iBin)/BinContents);
        alt_TimeDist->SetBinError(iBin,alt_TimeDist->GetBinError(iBin)/alt_TimeDist->GetBinWidth(iBin)/BinContents);
    }

    alt_TimeDist->Draw("");


}
void DrawSame(const char* aFileName, double up = 0, double low = 0)
{
    double speupper =  -0.025,
            spelower = -0.032;
    if(up != 0)
        speupper = up;
    if(low != 0)
        spelower = low;

    double trigger = 5e-9;
    TFile* tFile = (TFile*) gROOT->GetListOfFiles()->FindObject(aFileName);
    if(!tFile) tFile = new TFile(aFileName);

    //Reads NTuple from .fanat# file in ntp/
    TNtuple* ntp = (TNtuple*) tFile->Get("ntp");
    float* ntpCont = ntp->GetArgs();
    int nBinMax=1001;
    double bin[1000];
    int nBin=0;
    double upperBound=pow(10,nBin/20.);
    while(upperBound<10e9 && nBin<nBinMax){
        bin[nBin]= upperBound;
        nBin++;
        upperBound = pow(10,nBin/20.);
    }
    bin[nBin]=upperBound;

    TH1D * TD = new TH1D("TD","TD",nBin,bin);
    int nEntries = ntp->GetEntries();

    // read in the ntuple
    int iEntry=0;

    double hui = 0, pfui = 0;
    while(iEntry<nEntries){
        // >>> Look for trigger
        do{
            ntp->GetEntry(iEntry);
            iEntry++;
        }while(iEntry<nEntries &&
               ((ntpCont[8] > speupper) || (ntpCont[8] < spelower) || //single PE bump
                abs(ntpCont[9])>trigger || // trigger pulse
                ntpCont[22]<25));  // real pulse
        if(iEntry<nEntries){
            double tTriggerTime = ntpCont[1];
            double tTriggerPulseTime = ntpCont[9]+ntpCont[1];
            // >>> Check the next pulse
            ntp->GetEntry(iEntry);
            if(ntpCont[1]>=tTriggerTime){ // Do not account for the events at the sequence boundary
                do{
                    ntp->GetEntry(iEntry);
                    iEntry++;
                    ++pfui;
                }while(iEntry<nEntries &&
                       (ntpCont[22] < 25 ||
                        (ntpCont[8] > speupper))); // At least 1 PE
                if(iEntry<nEntries){
                    TD->Fill((ntpCont[9]+ntpCont[1]-tTriggerPulseTime)*1e9);
                    ++hui;
                }
            }
        }
    }
    cout << "hui: " << hui << "\t pfui: " << pfui << endl;



    double BinContents = TD->GetEntries();
    TD->Sumw2();
    for(int iBin=1; iBin<=TD->GetNbinsX(); iBin++)
    {
        if(iBin > 78 && iBin <84)
        {
            TD->SetBinContent(iBin,0);
            TD->SetBinError(iBin,0);
        }
        else{
            TD->SetBinContent(iBin,TD->GetBinContent(iBin)/TD->GetBinWidth(iBin)/BinContents);
            TD->SetBinError(iBin,TD->GetBinError(iBin)/TD->GetBinWidth(iBin)/BinContents);
        }
    }
    TD->SetDrawOption("E1");

    TD->Draw("same");

}
void FitTime(const char* aFileName, double up = 0, double low = 0, bool same = false)
{
    double speupper =  -0.01,
            spelower = -0.03;
    if(up != 0)
        speupper = up;
    if(low != 0)
        spelower = low;

    double trigger = 3e-9;
    TFile* tFile = (TFile*) gROOT->GetListOfFiles()->FindObject(aFileName);
    if(!tFile)
        tFile = new TFile(aFileName);

    //Reads NTuple from .fanat# file in ntp/
    TNtuple* ntp = (TNtuple*) tFile->Get("ntp");
    float* ntpCont = ntp->GetArgs();
    int nBinMax=1001;
    double bin[1000];
    int nBin=0;
    double upperBound=pow(10,nBin/20.);
    while(upperBound<5e9 && nBin<nBinMax){
        bin[nBin]= upperBound;
        nBin++;
        upperBound = pow(10,nBin/20.);
    }
    bin[nBin]=upperBound;

    TH1D * alt_TimeDist = new TH1D("alt_TimeDist","alt_TimeDist",nBin,bin);
    int nEntries = ntp->GetEntries();

    // read in the ntuple
    int iEntry=0;

    double hui = 0, pfui = 0;
    while(iEntry<nEntries){
        // >>> Look for trigger
        do{
            ntp->GetEntry(iEntry);
            iEntry++;
        }while(iEntry<nEntries &&
               ((ntpCont[8] > speupper) || (ntpCont[8] < spelower) || //single PE bump
                abs(ntpCont[9])>trigger || // trigger pulse
                ntpCont[22]<6));  // real pulse
        if(iEntry<nEntries){
            double tTriggerTime = ntpCont[1];
            double tTriggerPulseTime = ntpCont[9]+ntpCont[1];
            // >>> Check the next pulse
            ntp->GetEntry(iEntry);
            if(ntpCont[1]>=tTriggerTime){ // Do not account for the events at the sequence boundary
                do{
                    ntp->GetEntry(iEntry);
                    iEntry++;
                    ++pfui;
                }while(iEntry<nEntries &&
                       (ntpCont[22] < 6 ||
                        (ntpCont[8] > speupper))); // At least 1 PE
                if(iEntry<nEntries && (ntpCont[9]+ntpCont[1]-tTriggerPulseTime)>40e-9){
                    alt_TimeDist->Fill((ntpCont[9]+ntpCont[1]-tTriggerPulseTime)*1e9);
                    ++hui;
                }
            }
        }
    }
    cout << "Histogram created" << endl;



    double BinContents = alt_TimeDist->GetEntries();
    alt_TimeDist->Sumw2();
    for(int iBin=1; iBin<=alt_TimeDist->GetNbinsX(); iBin++)
    {
        if(iBin > 78 && iBin <84)
        {
            alt_TimeDist->SetBinContent(iBin,0);
            alt_TimeDist->SetBinError(iBin,0);
        }
        else{
            alt_TimeDist->SetBinContent(iBin,alt_TimeDist->GetBinContent(iBin)/alt_TimeDist->GetBinWidth(iBin)/BinContents);
            alt_TimeDist->SetBinError(iBin,alt_TimeDist->GetBinError(iBin)/alt_TimeDist->GetBinWidth(iBin)/BinContents);
        }
    }
    // set the gap to the right bins:
    GapStart = alt_TimeDist->GetXaxis()->GetBinUpEdge(78);
    GapEnd   = alt_TimeDist->GetXaxis()->GetBinLowEdge(84);

    TF1 * dn = new TF1("dn",funcDN, 5e6, 1e10,2);
    dn->SetParameter(0,5e-8);
    dn->SetParameter(1,0.2);
    alt_TimeDist->Fit(dn,"R");
    alt_TimeDist->Draw("");


    // try to fit the complete spectrum
    TF2 * cf = new TF1("cf",funcDNAP2WGapRecSimple, 100, 1e10, 6);
    cf->FixParameter(0,dn->GetParameter(0));
    cf->FixParameter(5,810e-9);
    cf->SetParameter(1,0.1);
    cf->SetParameter(2,1000);
    cf->SetParameter(3,0.1);
    cf->SetParameter(4,10e4);

    alt_TimeDist->Fit(cf,"R");
    //    cf->SetRange(200,1e9);
    cout << "2 AP" << endl;
    //alt_TimeDist->Fit(cf,"R");
    cout << "2 AP" << endl;
    TF1 * pcf = new TF1("pcf",funcDNAP3WGapRecSimple, 1e3,1e10,8);
    pcf->SetParameter(0,cf->GetParameter(0));
    pcf->SetParameter(1,0.01);
    pcf->SetParameter(2,2e3);
    pcf->SetParameter(3,cf->GetParameter(3));
    pcf->SetParameter(4,cf->GetParameter(4));
    pcf->FixParameter(5,cf->GetParameter(5));
    pcf->SetParameter(6,0.06);//cf->GetParameter(1));
    pcf->SetParameter(7,1e3); //cf->GetParameter(2));
    cout << "3 AP" << endl;
    alt_TimeDist->GetXaxis()->SetTitle(" #Delta t [ns]  ");
    alt_TimeDist->GetYaxis()->SetTitle("Probability");
    alt_TimeDist->GetXaxis()->SetLabelFont(40);
    alt_TimeDist->GetYaxis()->SetLabelFont(40);
    alt_TimeDist->GetXaxis()->SetTitleFont(40);
    alt_TimeDist->GetYaxis()->SetTitleFont(40);
    alt_TimeDist->GetXaxis()->SetTitleSize(0.05);
    alt_TimeDist->GetXaxis()->SetTitleOffset(1.1);
    alt_TimeDist->GetYaxis()->SetTitleSize(0.05);
    alt_TimeDist->GetXaxis()->SetLabelSize(0.05);
    alt_TimeDist->GetYaxis()->SetLabelSize(0.05);
    alt_TimeDist->GetYaxis()->SetRangeUser(1e-12,1e-3);
    alt_TimeDist->SetLineColor(3);
    alt_TimeDist->SetMarkerStyle(20);
    alt_TimeDist->SetMarkerColor(3);
    alt_TimeDist->SetDrawOption("E1");
    gStyle->SetOptStat(0);
    gStyle->SetEndErrorSize(5);
    alt_TimeDist->SetTitle("");
    alt_TimeDist->Fit(pcf,"R");

    cout << "3 AP" << endl;
    pcf->SetRange(800,1e10);

    alt_TimeDist->Draw("E1");
    //pcf->Draw("same");


}
void plotrecovery(const char* aFileName, double up = 0, double low = 0)
{
    TFile* tFile = (TFile*) gROOT->GetListOfFiles()->FindObject(aFileName);
    if(!tFile) tFile = new TFile(aFileName);
    double speupper =  -0.01,
            spelower = -0.03;
    if(up != 0)
        speupper = up;
    if(low != 0)
        spelower = low;

    //Reads NTuple from .fanat# file in ntp/
    TNtuple* ntp = (TNtuple*) tFile->Get("ntp");
    float* ntpCont = ntp->GetArgs();
    int nEntries = ntp->GetEntries();
    int i = 0;
    TH2D* h = new TH2D("h","h", 500,0,.1e-6,500,-0.1,0.01);
    while(i<nEntries)
    {
        ntp->GetEntry(i);
        // check for events with at least 2 pulses
        if(ntpCont[4]>1)
        {
            if(ntpCont[8]<up && ntpCont[8]>low)
            {
                int event = ntpCont[0];
                while(ntpCont[0]==event && i<nEntries )
                {
                    if((ntpCont[16]/ntpCont[17])==1)
                            h->Fill(ntpCont[9],ntpCont[8]);
                    i++;
                    ntp->GetEntry(i);

                }
            }
            else{++i;}
        }
        else{
            ++i;
        }

    }
    TCanvas * c1 = new TCanvas("c1","c1",200,10,700,500);
    c1->cd();
    c1->SetLogz();
    h->Draw("colz");
}
void FitTime_2(const char* aFileName, double up = 0, double low = 0)
{
    double speupper =  -0.01,
            spelower = -0.03;
    if(up != 0)
        speupper = up;
    if(low != 0)
        spelower = low;

    double trigger = 1e-9;
    TFile* tFile = (TFile*) gROOT->GetListOfFiles()->FindObject(aFileName);
    if(!tFile) tFile = new TFile(aFileName);

    //Reads NTuple from .fanat# file in ntp/
    TNtuple* ntp = (TNtuple*) tFile->Get("ntp");
    float* ntpCont = ntp->GetArgs();
    int nBinMax=1001;
    double bin[1000];
    int nBin=0;
    double upperBound=pow(10,nBin/20.);
    while(upperBound<5e9 && nBin<nBinMax){
        bin[nBin]= upperBound;
        nBin++;
        upperBound = pow(10,nBin/20.);
    }
    bin[nBin]=upperBound;

    TH1D * alt_TimeDist = new TH1D("alt_TimeDist","alt_TimeDist",nBin,bin);
    int nEntries = ntp->GetEntries();

    // read in the ntuple
    int iEntry=0;


    while(iEntry<nEntries){
        // >>> Look for trigger
        do{
            ntp->GetEntry(iEntry);
            iEntry++;
        }while(iEntry<nEntries &&
               ((ntpCont[12] > speupper) || (ntpCont[12] < spelower) || //single PE bump
                abs(ntpCont[13])>trigger || // trigger pulse
                ntpCont[10]<6));  // real pulse
        if(iEntry<nEntries){
            double tTriggerTime = ntpCont[1];
            double tTriggerPulseTime = ntpCont[13]+ntpCont[1];
            // >>> Check the next pulse
            ntp->GetEntry(iEntry);
            if(ntpCont[1]>=tTriggerTime){ // Do not account for the events at the sequence boundary
                do{
                    ntp->GetEntry(iEntry);
                    iEntry++;
                }while(iEntry<nEntries &&
                       (ntpCont[10] < 6 ||
                        (ntpCont[12] > speupper))); // At least 1 PE
                if(iEntry<nEntries &&
                        (ntpCont[13]+ntpCont[1]-tTriggerPulseTime)>40e-9){
                    alt_TimeDist->Fill((ntpCont[13]+ntpCont[1]-tTriggerPulseTime)*1e9);
                }
            }
        }
    }



    double BinContents = alt_TimeDist->GetEntries();
    alt_TimeDist->Sumw2();
    for(int iBin=1; iBin<=alt_TimeDist->GetNbinsX(); iBin++)
    {
        if(iBin > 78 && iBin <84)
        {
            alt_TimeDist->SetBinContent(iBin,0);
            alt_TimeDist->SetBinError(iBin,0);
        }
        else{
            alt_TimeDist->SetBinContent(iBin,alt_TimeDist->GetBinContent(iBin)/alt_TimeDist->GetBinWidth(iBin)/BinContents);
            alt_TimeDist->SetBinError(iBin,alt_TimeDist->GetBinError(iBin)/alt_TimeDist->GetBinWidth(iBin)/BinContents);
        }
    }
    // set the gap to the right bins:
    GapStart = alt_TimeDist->GetXaxis()->GetBinUpEdge(78);
    GapEnd   = alt_TimeDist->GetXaxis()->GetBinLowEdge(84);
    if(true)
    {
        TF1 * dn = new TF1("dn",funcDN, 1e7, 1e10,2);
        dn->SetParameter(0,5e-8);
        dn->SetParameter(1,0.2);
        alt_TimeDist->Fit(dn,"R");


        // try to fit the complete spectrum
        TF2 * cf = new TF1("cf",funcDNAP2WGapRecSimple, 100, 1e10, 6);
        cf->FixParameter(0,dn->GetParameter(0));
        cf->FixParameter(5,810e-9);
        //cf->SetParLimits(5,790e-9,830e-9);
        cf->SetParameter(1,0.1);
        cf->SetParameter(2,1e3);
        cf->SetParameter(3,0.1);
        cf->SetParameter(4,1e5);

        cout << "2 AP" << endl;
        alt_TimeDist->Fit(cf,"R");
        cout << "2 AP" << endl;
        TF1 * pcf = new TF1("pcf",funcDNAP3WGapRecSimple, 500,1e10,8);
        pcf->FixParameter(0,cf->GetParameter(0));
        pcf->FixParameter(1,cf->GetParameter(1));
        pcf->FixParameter(2,cf->GetParameter(2));
        pcf->FixParameter(3,cf->GetParameter(3));
        pcf->FixParameter(4,cf->GetParameter(4));
        pcf->FixParameter(5,cf->GetParameter(5));
        pcf->SetParameter(6,0.01);
        pcf->SetParLimits(6,0.01,0.2);
        pcf->SetParameter(7,800);
        pcf->SetParLimits(7,500,900);
        //    cout << "3 AP" << endl;
        //    alt_TimeDist->Fit(pcf,"R");
        //    cout << "3 AP" << endl;
        //    pcf->Draw("same");
        cf->Draw("same");
    }
}
