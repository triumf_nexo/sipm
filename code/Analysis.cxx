#include <sys/stat.h>
#include <iostream>
#include <cstdlib>
#include <cmath>

#include "ntpAnalysis.h"

void Analyse(ntpAnalysis analysis, int aRun, int aRun2);

int main(int argc, char** argv)
{
	//possible to combine two runs with different timing resolutions
	int aRun = argc>1 ? atoi(argv[1]) : 0;
	int aRun2 = argc>2 ? atoi(argv[2]) : 0;
	
	if(argc == 2)
	{
		ntpAnalysis analysis (aRun);
		Analyse(analysis, aRun, 0);
	}
	else if(argc == 3) 
	{
		ntpAnalysis analysis (aRun, aRun2);
		Analyse(analysis, aRun, aRun2);
	}
	else
	{
		std::cout << "Usage: Analysis.exe [run#1] [run#2]. Run#2 optional. See nEXO/RunInfo.txt for run numbers." << std::endl;
		exit(1);
	}
}

void Analyse(ntpAnalysis analysis, int aRun, int aRun2)
{
	analysis.calculateAllParameters(aRun, aRun2, 6e-9);
	analysis.writeAll();
	//	ntpAnalysis.makePlots(1);
}
