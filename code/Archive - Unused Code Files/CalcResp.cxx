#include "Waveform.h"
#include "PulseFinder.h"
#include "SetupParameter.h"
#include "WfmFile.h"

#include "TF1.h"

#include <iostream>
#include <cmath>
#include <cstdlib>

double Func2Exp(double* x, double* par){
  if(x[0]<par[0]) return 0.;
  return par[1]*(1.-exp(-(x[0]-par[0])/par[2]))*exp(-(x[0]-par[0])/par[3]);
    
}

int main(int argc, char** argv){
  //__________________________________________________________________________
  // --- Parameters
  int aRun = 1;
  if(argc>1) aRun = atoi(argv[1]);
  const char* aLog = "RunInfo.txt";
  if(argc>2) aLog = argv[2];
  std::cout << "reading parameters" << std::endl;
  SetupParameter* setupPar = SetupParameter::instance(aLog,aRun);
  std::cout << "done reading parameters" << std::endl;

  //__________________________________________________________________________
  // --- Init pulse finder0
  //V1729Data respDataReader(aRun,100000,setupPar->Dir); 
  DataFile* dataFile = new WfmFile(setupPar->getFilename(), setupPar->Run,
      setupPar->FileCount, 1e9, 1000);
  if (!dataFile->isGood()) {
    std::cerr << "Error: invalid filename: " << setupPar->getFilename() <<
      "with count: " << setupPar->FileCount << std::endl;
  }
  
  //respDataReader.setWFTimeOffset(0,50);
  //respDataReader.setWFTimeOffset(1,50);
  //respDataReader.setDiffWF(1,0);
  dataFile->getWaveform(0)->setBaselineLimits(setupPar->BaselineMin,
						  setupPar->BaselineMax,
						  setupPar->SigMin,
						  setupPar->SigMax);
  dataFile->getWaveform(0)->resetAverage();
  //respDataReader.setSecondOrderVernier();

  //respDataReader.setDefaultWaveform(1);
  //respDataReader.getWaveform()->setNegPolarity();
  TF1 f2Exp("f2Exp",Func2Exp,0.,5000.,4);
  f2Exp.SetParameters(0.,1.,0.8,23.);
  double scale = f2Exp.Integral(0.,2000.);
  f2Exp.SetParameter(1,1./scale);
  //respDataReader.getWaveform()->setFilter(f2Exp);

  PulseFinder* pulseFinder = PulseFinder::instance();
  pulseFinder->calcResp(dataFile,
			setupPar->IntRangeMin,
			setupPar->IntRangeMax,
			setupPar->IntMin,
			setupPar->IntMax,
			setupPar->PTimeMin,
			setupPar->PTimeMax,1);
			//(setupPar->Type==1 || setupPar->Type==2)? 1: 0);
 
};
