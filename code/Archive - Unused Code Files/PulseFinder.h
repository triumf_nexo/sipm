#ifndef PulseFinder_h
#define PulseFinder_h

class TH1D;
class TGraph;
class DataFile;

#include <vector>

#include "Pulse.h"
#include "Waveform.h"

//#include "TList.h"
#include "TMinuit.h"


class PulseFinder{
 public:
  static PulseFinder* instance(int aNFitPulseMax=10);

  // initialize response function
  void calcResp(DataFile* dataFile, 
		int aFirstBin,int aLastBin,
		double aMinInt,double aMaxInt,
		double aPTimeMin,double aPTimeMax,
		int aNoTimeAdjustment=1,
		int aFirstEvent=0,int aLastEvent=0);
  void calcRespPass2(DataFile* aRespData, 
		     int aFirstBin,int aLastBin,
		     double aMinInt,double aMaxInt,
		     double aPTimeMin,double aPTimeMax);
  void readResp(int aAvRun, double aScale, int aPass=1);
  double getPulseTime(Waveform* aWF, int aFirstBin, int aLastBin);
  void buildRespInterpolater(int aFirstBin,int aLastBin);
  TH1D* getResp() {return mWFResp;}

    // set pulse finder parameters
  int setParameters(int aLowFitRange,int aHighFitRange,
		    double aNoiseCut,
		    double aCLMin);
  void setMask(int aMaskMin, int aMaskMax)
  {mMaskMin=aMaskMin;mMaskMax=aMaskMax;}
  
  // find pulse
  int findPulseSimple(Waveform* aWF);
  int findPulseFit(Waveform* aWF);
  int findPulse(Waveform* aWF); // combines first two: simple then fit
  TGraph* getPulseGraph(); 
  const Pulse& getPulse(int aIndex){return mPulses[aIndex];}
  TH1D* getFitWaveform(){ return mFitWaveform;}
  TH1D* histFitFunction(char* aName = "HFF");

  void minuitFunction(Int_t &npar, Double_t *gin, 
		      Double_t &f, Double_t *par, Int_t iflag);
  
 private:
  // Singleton stuff
  static PulseFinder* mInstance;
  PulseFinder(int aNFitPulseMax);

  // Pulse parameter
  int mRiseTime;
  int mLowFitRange;
  int mHighFitRange;
  double mNoiseCut;
  double mCLMin;
  double mNoise;

  PulseArray mPulses;


  TH1D* mWFResp;
  TH1D* mFitWaveform;
  Waveform* mWF;
  TMinuit* mMinuit;
  double mBaseline;
  int mNFitPulse;
  int mNFitPulseMax;
  int mFitLowBin;
  int mFitHighBin;

  // average function
  double mYItrp[4096];
  double mRespMax;
  int mNNegBinItrp;
  double getResp(double aX);  
  double calcWF(double* x, double* par);
  int getMaxChi2Bin(double* par);

  int mMaskMin;
  int mMaskMax;

};

#endif
