#include <fstream>
#include <iostream>
#include <cstring>
#include <cstdlib>

#include "SetupParameter.h"

SetupParameter* SetupParameter::mPar = 0;

SetupParameter::SetupParameter(const char* aFileName, int aRun){
  std::ifstream fIn(aFileName);
  char tDum[50];
  char tType[50];
  char tCTemp[50];
  while(!fIn.eof() && fIn.get() != '\n');
  //cout << tDum << endl;
  fIn >> Run;
  //cout << Run << endl;
  while(Run!=aRun && !fIn.eof()){  
    while(!fIn.eof() && fIn.get() != '\n');
    while(fIn.peek() == '\n') {
      fIn.get();
    }
    if (fIn.peek() == '#') {
      continue;
    }
    fIn >> Run;
    //cout << Run << endl;
  }
  if(fIn.eof()){
    std::cerr << "Cannot find run number "  << aRun << " in setup file "
	      << aFileName << std::endl;
    exit(1);
  }
  else{    
    fIn >> Date >> Device >> Temp >> Wlen >> Voltage >> FNameFormat >>
      WaveformChannel >> TriggerChannel >> FileCount >> AvRun >> AvScale >>
      BaselineMin >> BaselineMax >> SigMin >> SigMax >> Max1peMin >> Max1peMax
      >> PulseSlopeThreshold >> TriggerLevel >> IntRangeMin >> IntRangeMax >>
      IntMin >> IntMax >> PTimeMin >> PTimeMax >> LowFitRange >> HighFitRange >>
      SNMin >> Chi2OverNDoFMax >> MaskMin >> MaskMax;
  }
  std::cout << "Setup parameters read from file " << aFileName << "\nrun: " <<
   Run << "\ndate: " << Date << "\ndevice: " << Device << "\nWlen: " << Wlen <<
   "\ntemp: " << Temp << "\nvoltage: " << Voltage << "\nfile count: " <<
   FileCount << std::endl;
}

const char* SetupParameter::getFilename(bool triggerFile) {
  const char* channel = triggerFile ? TriggerChannel : WaveformChannel;
  // need to setup the file name
  switch (FNameFormat) {
    case 1:
      sprintf(filename,
          "data/%d-%dV%dC/%dnm%%03d_%s.wfm",
          Date, Voltage, Temp, Wlen, channel);
      break;
    case 2:
      sprintf(filename,
          "data/%d-%dnm%dC/%dV_%s.wfm",
          Date, Wlen, Temp, Voltage, channel);
      break;
    case 3:
      sprintf(filename,
          "data/%d-%dnm%dC/%dV-long%%d_%s.wfm",
          Date, Wlen, Temp, Voltage, channel);
      break;
    case 4:
      sprintf(filename,
          "data/%d-PMT/%dnm-%%d_%s.wfm",
          Date, Wlen, channel);
      break;
    case 5:
      sprintf(filename,
          "data/%d-H6152/%dnm-%%d_%s.wfm",
          Date, Wlen, channel);
      break;
    case 6:
      sprintf(filename,
          "data/%d-%s-%dnm%dC/%dV_%s.wfm",
          Date, Device, Wlen, Temp, Voltage, channel);
      break;
    case 7:
      sprintf(filename,
          "data/%d-%s-%dnm%dC/%dV%%03d_%s.wfm",
          Date, Device, Wlen, Temp, Voltage, channel);
      break;
    default:
      std::cerr << "Unrecognized file format: " << FNameFormat << std::endl;
      exit(1);
  }
  return filename;
}
