/*******************************************************************************
* Distribution.cxx
*
* Description:
* Reads a wfm file and produces an ntuple with the distribution of the pulses
* for waveforms in that wfm file.
*
* History:
* v0.1  2011/12/14  Initial file (Kyle Boone, kyboone@gmail.com)
*******************************************************************************/

#include <sys/stat.h>
#include <iostream>
#include <cstdlib>

#include "DistributionFinder.h"
#include "SetupParameter.h"
#include "DataFile.h"
#include "WfmFile.h"
#include "LecroyFile.h"
#include "Waveform.h"

#include "TFile.h"
#include "TNtuple.h"
#include "TF1.h"

// Parameters



double FuncExpGaus(double* x, double*p){
 // p[0]: gaussian mu
  // p[1]: amplitude
  // p[2]: gaussian sig
  // p[3]: exponential decay constant
	// p[4]: baseline
  // convolution of an exponential and a gaussian, formula found in a
  // chromatography paper on exponentially modified gaussians.
  // http://www.springerlink.com/content/yx7554182g164612/
	double time = x[0]-p[0];
  return p[1]*exp(1/2*p[2]*p[2]/p[3]/p[3]-time/p[3])
    * (TMath::Erf(1/sqrt(2)*(p[0]/p[2]+p[2]/p[3]))
			 + TMath::Erf(1/sqrt(2)*(time/p[2]-p[2]/p[3])))+p[4];
}

int main(int argc, char** argv){
	int aRun = argc>1 ? atoi(argv[1]) : 0;
	int aTraining = argc>2 ? atoi(argv[2]) : 0;
	int aNEventMax = argc>3 ? atoi(argv[3]) : 100000000;

	// --- Open run info file
	ifstream parFile("RunInfo.txt");
	char buf[200];
	for(int iBuf=0; iBuf<9; iBuf++) parFile >> buf; 
	int run;	
	char fileName[200];
	double WFAmpBining;
	double ampMin;
	double ampMax;
	double diffAmpThresh;
	double riseTimeSig;
	double fallTimeTau;
	int polarity;
	parFile >> run >> fileName >>  WFAmpBining >> ampMin >> ampMax 
					>> diffAmpThresh >> riseTimeSig >> fallTimeTau >> polarity;
	while(!parFile.eof() && run!=aRun){
		parFile >> run >> fileName >>  WFAmpBining >> ampMin >> ampMax 
						>> diffAmpThresh >> riseTimeSig >> fallTimeTau >> polarity;
	}
	if(parFile.eof()){
		std::cerr << "Could not find run requested " << aRun << std::endl;
	}

	// --- Open input file
	LecroyFile dataFile("Data/20C_new_405_65-63_500ns_0.root");
	std::cout << "Waveform found : " << dataFile.getWaveformCount() << std::endl;
	int nEvent = aNEventMax<dataFile.getWaveformCount() ? aNEventMax : dataFile.getWaveformCount();

	// --- Open output file
	char outFileName[200];
	sprintf(outFileName,"%s.anat%i",fileName,aTraining);
	TFile outFile(outFileName ,"RECREATE");
	TNtuple ntp("ntp","ntp","evt:wf:blmu:blcount:np:ampp:tp:ap:tr:tf:b:chi2");


	// --- Create histogram for baseline calculation
	TH1F HBaseline("HBaseline","HBaseline",
								 (int)((ampMax-ampMin)/WFAmpBining),ampMin,ampMax);

	// --- Function for pulse fitting
	TF1 FExpGaus("FExpGaus",FuncExpGaus,0.,0.5e-6,5); // range reset later

	// --- Start loop over events
	for(int iEvent=0; iEvent<nEvent; iEvent++){
		Waveform* wf = dataFile.getWaveform(iEvent);
		int nBinRiseTime = riseTimeSig*3/wf->GetBinWidth(1);
		int nBinFallTime = fallTimeTau*3/wf->GetBinWidth(1);
		char ciwf[50];
		strcpy(ciwf,wf->GetName()+7);
		int iwf=atoi(ciwf);
		
		// --- Calculate baseline
		HBaseline.Reset("C");
		for(int iBin=1; iBin<=wf->GetNbinsX(); iBin++){
			HBaseline.Fill(wf->GetBinContent(iBin));
		}
		int blMostProbBin=0;
		int blMostProbCont=0;
		for(int iBin=1; iBin<=HBaseline.GetNbinsX(); iBin++){
			if(HBaseline.GetBinContent(iBin)>blMostProbCont){
				blMostProbCont=HBaseline.GetBinContent(iBin);
				blMostProbBin=iBin;
			}				
		}
		double blMu=HBaseline.GetBinLowEdge(blMostProbBin);
		//>>> look for the bin corresponding to >99% of the counts 
		//double nonPulseCount= negPolarity? 
		//HBaseline.Integral(blMostProbBin,HBaseline.GetNbinsX()) :
		//HBaseline.Integral(1,blMostProbBin);
		//while();

		// --- Look for pulses
		int nPulse=0;
		for(int iBin=nBinRiseTime+1; iBin<=wf->GetNbinsX(); iBin++){
			if(polarity*(wf->GetBinContent(iBin)-wf->GetBinContent(iBin-nBinRiseTime))>diffAmpThresh){
				double amp=polarity*(wf->GetBinContent(iBin));
				iBin++;
				while(iBin<=wf->GetNbinsX() && 
							amp<polarity*(wf->GetBinContent(iBin))){
					amp=polarity*(wf->GetBinContent(iBin));
					iBin++;
				}
				if(iBin<wf->GetNbinsX()){
					nPulse++;
					iBin--;
					FExpGaus.SetRange(wf->GetBinCenter(iBin)-riseTimeSig*5-10e-9,
														wf->GetBinCenter(iBin)+fallTimeTau*3);
					FExpGaus.SetParameter(0,wf->GetBinCenter(iBin));
					FExpGaus.SetParameter(1,(wf->GetBinContent(iBin)-blMu)/2.);//not sure
					if(aTraining==10){
						FExpGaus.SetParameter(2,riseTimeSig);
						FExpGaus.SetParameter(3,fallTimeTau);
						FExpGaus.FixParameter(4,blMu);
					}
					else{
						FExpGaus.FixParameter(2,riseTimeSig);
						FExpGaus.FixParameter(3,fallTimeTau);
						FExpGaus.SetParameter(4,blMu);
					}					
					wf->Fit("FExpGaus","QR0");
					ntp.Fill(iEvent,iwf,
									 blMu,blMostProbCont,
									 nPulse,
									 amp,
									 FExpGaus.GetParameter(0),
									 FExpGaus.GetParameter(1),
									 FExpGaus.GetParameter(2),
									 FExpGaus.GetParameter(3),
									 FExpGaus.GetParameter(4),
									 FExpGaus.GetChisquare());
					iBin+=nBinFallTime;
				}
			}
		}
		if(!nPulse){
			ntp.Fill(iEvent,blMu,blMostProbCont,0);
		}

		if(iEvent%100==0){
			std::cout << iEvent << " " << blMu << " " << nPulse << std::endl;
		}
		
	}

	std::cout << "Writing output in " << outFileName << std::endl;
	ntp.Write();
}



//double blRMS=HBaseline.GetRMS();
		//FGaus.SetRange(blMostProb-blRMS*3.,blMostProb+blRMS*3);
		//FGaus.SetParameters(blMostProbNb,blMostProb,blRMS);
		//HBaseline.Fit("FGaus","QR0");
		//ntp.Fill(iEvent,blMostProb,FGaus.GetParameter(2),FGaus.GetParameter(1));//blMostProbNb,blMostProb);



/*

  if (argc < 2) {
    printf("Usage: %s RUN [MAX EVENT]\n", argv[0]);
    return 1;
  }

  const char* runInfoFilename = "RunInfo.txt";
  int run = atoi(argv[1]);
  int maxEvent = argc>2 ? atoi(argv[2]) : -1;

  SetupParameter* setupPar =  SetupParameter::instance(runInfoFilename, run);
  
  // Setup distribution finder
  DistributionFinder* distributionFinder = new
    DistributionFinder(setupPar->PTimeMin, setupPar->Max1peMin,
        setupPar->Max1peMax, setupPar->PulseSlopeThreshold,
        setupPar->TriggerLevel);

  //__________________________________________________________________________
  // --- Open data files for trigger and signal

  DataFile* signalDataFile = new WfmFile(setupPar->getFilename(), setupPar->Run,
      setupPar->FileCount, "HWF%i", 1e9, 1);

  if (!signalDataFile->isGood()) return 1;

		//DataFile* triggerDataFile = new WfmFile(setupPar->getFilename(true),
     //setupPar->Run, setupPar->FileCount, "HWFTrigger%i", 1e9, 1);
  //if (!signalDataFile->isGood() || !triggerDataFile->isGood()) {
		// return 2;

  Waveform* WF = signalDataFile->getWaveform(0);
  WF->setBaselineLimits(setupPar->BaselineMin,
			setupPar->BaselineMax,
			setupPar->SigMin,
			setupPar->SigMax);

  int NEvent = signalDataFile->getWaveformCount();
  if (NEvent > maxEvent && maxEvent != -1) {
    NEvent=maxEvent;
  }

  //__________________________________________________________________________
  // --- Open output file
  mkdir("./distribution", 0775);
  char directory[200];
  sprintf(directory, "./distribution/%s-%dnm-%dC", setupPar->Device, setupPar->Wlen, setupPar->Temp);
  mkdir(directory, 0775);
  char OutFileName[200];
  sprintf(OutFileName, "%s/%dV.root", directory, setupPar->Voltage);
  TFile OutFile(OutFileName, "RECREATE");


  //__________________________________________________________________________
  // ---
  for(int ti=0; ti<NEvent; ti++){
    if (ti%1000==0) {
      std::cout << "Processing event #: " << ti << std::endl;
    }
    distributionFinder->processEvent(signalDataFile->getWaveform(ti))
			 //triggerDataFile->getWaveform(ti));
  }
  
  // Write the results of the distribution finder to the file
  distributionFinder->write();

  // Append the FWHM results to a file
  //double fwhm, fwhmError;
  //distributionFinder->getFwhmResults(&fwhm, &fwhmError);
  //std::ofstream fwhmFile("./fwhm-results.txt", std::ios::app);
  //fwhmFile << setupPar->Run << "\t" << setupPar->Device << "\t" <<
	//setupPar->Temp << "\t" << setupPar->Wlen << "\t" << setupPar->Voltage / 10.
	//<< "\t" << fwhm << "\t" << fwhmError << std::endl;

  // Clean up
  delete signalDataFile;
  delete distributionFinder;
  OutFile.Close();
  std::cout << "Results written to " << OutFileName << std::endl;
}
*/
