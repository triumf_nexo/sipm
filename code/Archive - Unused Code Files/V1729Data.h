#ifndef V1729Data_h
#define V1729Data_h

class TFile;
class TTree;
class Waveform;
class TProfile;

struct WFData{
  Int_t NBin;
  Int_t WF[10245]; 
};

class V1729Data{
 public:
  V1729Data(int aRun, int aNEventMax=1000000, char* aDir="data");
  ~V1729Data();
  void setDefaultWaveform(int aIndex){mDefaultIndex = aIndex;}
  void setDiffWF(int aIndexP, int aIndexN);
  void setSecondOrderVernier();


  Waveform* getWaveform();
  Waveform* getWaveform(int aIndex){return mWF[aIndex];}
  int getNWaveforms(){return mNWF;}
  void getEntry(int aIndex);
  int getNEvent(){return mNEvent;}
  int getRun(){return mRun;}

  int getVernier(){return mWFData.WF[1];}

  void setWFTimeOffset(int aCh, int aOffset){mWFTimeOffset[aCh]=aOffset;}

  static int mTimeBinShift;
  static int mLowVernier;
  static int mHighVernier;
  static double mVernierWidth;
  static int mV1729WFSize;
  static int mWFTimeOffsetDefault;
  int mWFTimeOffset[4];
  static int mWFIdealHistMax;

 private:
  WFData mWFData;
  int mNWF;
  Waveform** mWF;
  TFile* mFile;
  TTree* mTree;
  int mNEvent;
  int mRun;
  TProfile* mVernierProfile;

  Waveform* mWFDiff;
  int miWFDiffP;
  int miWFDiffN;

  int mDefaultIndex;

};

#endif
