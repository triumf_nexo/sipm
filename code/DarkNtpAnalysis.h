#ifndef DarkNtpAnalysis_h
#define DarkNtpAnalysis_h

class TF1;
class TH1F;
class TH1D;

class DarkNtpAnalysis{

 public:
	DarkNtpAnalysis(int run); //Initialiser

	//Public functions
	void ProcessDTime(bool aSaveGraph);
	void ProcessXTalk(bool aSaveGraph, double one_pe_upperthresh, double two_pe_lowerthresh);
	void WriteStats();
	void alt_processTime();
    void createTimingHistogram();

 private: 
 	//Private functions
	void init(int run);
	char* getFileName();

	//Vars from config file
	int mRun;
	int mFitType;

	//File paths
	std::string strRunInfo;
	std::string strSavePath;
	std::string strXTalkSavePath;

	//File path pointers
	char* mFileName;
	char* mRunInfo;
	char* mSavePath;
	char* mXTalkSavePath;

	//Histograms
	TH1D* TimeDist;
	TH1D* XTalkDist;
	
	TH1D* alt_TimeDist;

	//Data Statistics
	double XTalkThresh;
	double XTalkProb;
	double DNRate;
	double APulseRate;

	//Data Statistic Errors
	double XTalkThresh_E;
	double XTalkProb_E;
	double DNRate_E;
	double APulseRate_E;

    struct NtpCont{
        float evt; //event number
        float tt;
        float blmu; //baseline mean for waveform
        float blRMS; //baseline RMS
        float np; //number of pulses in an event
        float pa; //pulse amplitude
        float pt; //pulse time
        float tchi2; //SPTemplateChi2
        float fa; //fit amplitude
        float ft; //fit time
        float frt; //fit rise time
        float fft; //fit fall time
        float fft2;
        float fblmu; //fit baseline mean
        float fchi2; //chi2
        float ndf; //number degrees of freedom
        float frchi2; //chi2 for refit
        float frndf; //number degrees of freedom for refit
        float pr;
        float nl;
        float paa;
        float pq;
        float pw;
    };

};

#endif
