#include <sys/stat.h>
#include <iostream>
#include <cstdlib>
#include <cmath>

#include "TF1.h"
#include "TROOT.h"
#include "TH1.h"
#include "TGraph.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TLegend.h"

#include "DarkNtpAnalysis.h"

int main(int argc, char** argv)
{
	double upperlim;
	double lowerlim;

	int aRun = argc>1 ? atoi(argv[1]) : 0; //Run number to be processed

	if(argc == 2) //If only a run number is supplied, these amplitude limits are used.
	{
		upperlim = -0.025;
		lowerlim = -0.08;
	}
	else if(argc == 3) /*This allows the run number and brand keyword to be used for easily
						going between different brands*/
	{
		std::string brand = argv[2];
		if (brand == "ktk")
		{
			upperlim = -0.005;
			lowerlim = -0.04;
		}
		else if (brand == "hmt")
		{
			upperlim = -0.025;
			lowerlim = -0.08;
		}
		else
		{
			std::cout << "Invalid brand name given. See source code for valid values." << std::endl;
			exit(1);
		}
	}
	else if (argc == 4) //This allows the upper and lower limits to be specified manually.
	{
		upperlim = atof(argv[2]);
		lowerlim = atof(argv[3]);
	}
	else
	{
		std::cout << "Usage: DarkAnalysis.exe [run] OR DarkAnalysis.exe [run] [brand] OR DarkAnalysis.exe [run] [upperlim] [lowerlim]. See RunInfo.txt for run numbers." << std::endl;
		exit(1);
	}

	DarkNtpAnalysis analysis(aRun);
    //analysis.createTimingHistogram();
    //analysis.ProcessDTime(1); //Plots the delta T distribution and extracts values
    analysis.alt_processTime();
    //analysis.ProcessXTalk(1, upperlim, lowerlim); //Calculates X talk probability, and threshold - uses an upper and lower limit on the 1st and 2nd pe peaks:
    //analysis.WriteStats(); //Writes the statistics to file
}

