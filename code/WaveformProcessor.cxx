/*******************************************************************************
* Distribution.cxx
*
* Description:
* Reads a wfm file and produces an ntuple with the distribution of the pulses
* for waveforms in that wfm file.
*
* History:
* v0.1  2011/12/14  Initial file (Kyle Boone, kyboone@gmail.com)
*******************************************************************************/

#include <sys/stat.h>
#include <iostream>
#include <cstdlib>
#include <cmath>

#include "DataFile.h"
//#include "WfmFile.h"
#include "LecroyFile.h"
#include "LecroyHdfFile.h"
#include "WaveformProcessor.h"
#include "Waveform.h"


#include "TFile.h"
#include "TF1.h"
#include "TROOT.h"
#include "TH1.h"
#include "TGraph.h"
#include "TMath.h"

//___________________________________________________________________
// --- Fit function
double FuncExpGausMulti(double* x, double*p){
    // p[0]: baseline
    // p[1]: gaussian sig
    // p[2]: exponential decay constant
    // p[3]: number of pulses
    // p[4+2*i]: pulse[i] amplitude
    // p[5+2*i]: pulse[i] time
    // convolution of an exponential and a gaussian, formula found in a
    // chromatography paper on exponentially modified gaussians.
    // http://www.springerlink.com/content/yx7554182g164612/
    double val=p[0];
    for(int iPulse=0; iPulse<p[3]; iPulse++){
        double time = x[0]-p[5+2*iPulse];
        if(((p[1]*p[1]/p[2]/2.-time)/p[2])<700){ //otherwise exponential explodes
            val+=p[4+2*iPulse]/2.*//p[2]/2.*
                    exp((p[1]*p[1]/p[2]/2.-time)/p[2])*
                    TMath::Erfc((p[1]*p[1]/p[2]-time)/sqrt(2)/p[1]);
        }
        //exp(1/2*p[1]*p[1]/p[2]/p[2]-time/p[2])*
        //(TMath::Erf(1/sqrt(2)*(p[5+2*iPulse]/p[1]+p[1]/p[2]))+
        // TMath::Erf(1/sqrt(2)*(time/p[1]-p[1]/p[2])));
    }
    return val;
}
double FuncExpGausMulti2(double* x, double*p){
    // p[0]: baseline
    // p[1]: gaussian sig
    // p[2]: first  exponential decay constant
    // p[3]: second exponential decay constant
    // p[4]: peak ratio
    // p[5]: activate second TC set to 1 for use both TCs, 0 otherwise
    // p[6]: number of pulses
    // p[7+2*i]: pulse[i] amplitude
    // p[8+2*i]: pulse[i] time
    double val=p[0];
    if(p[5]==1)
    {
        for(int iPulse=0; iPulse<p[6]; iPulse++){
            double time = x[0]-p[8+2*iPulse];
            if(((p[1]*p[1]/p[2]/2.-time)/p[2])<700){ //otherwise exponential explodes
                val+=p[4]*p[7+2*iPulse]/2.*
                        exp((p[1]*p[1]/p[2]/2.-time)/p[2])*
                        TMath::Erfc((p[1]*p[1]/p[2]-time)/sqrt(2)/p[1]);
            }
            if(((p[1]*p[1]/p[3]/2.-time)/p[3])<700){ //otherwise exponential explodes
                val+=(1-p[4])*p[7+2*iPulse]/2.*
                        exp((p[1]*p[1]/p[3]/2.-time)/p[3])*
                        TMath::Erfc((p[1]*p[1]/p[3]-time)/sqrt(2)/p[1]);
            }
        }
    }
    else if(p[5]== 0)
    {
        for(int iPulse=0; iPulse<p[6]; iPulse++){
            double time = x[0]-p[8+2*iPulse];
            if(((p[1]*p[1]/p[2]/2.-time)/p[2])<700){ //otherwise exponential explodes
                val+=p[7+2*iPulse]/2.*
                        exp((p[1]*p[1]/p[2]/2.-time)/p[2])*
                        TMath::Erfc((p[1]*p[1]/p[2]-time)/sqrt(2)/p[1]);
            }
        }
    }
    else{std::cout << "SOMETHINGS WRONG!" <<p[5]<<std::endl;}
    return val;
}

//___________________________________________________________________
// --- Constuctor
WaveformProcessor::WaveformProcessor(const char* aSetupFileName, int aRun,  int numPulsesFit){
    init(aSetupFileName,aRun,numPulsesFit);
}
// ---
void WaveformProcessor::init(const char* aSetupFileName, int aRun,  int numPulsesFit){
    std::cout << "test" << std::endl;
    strcpy(mSetupFileName,aSetupFileName);
    // --- Open run info file
    ifstream parFile(aSetupFileName);
    char buf[200];
    for(int iBuf=0; iBuf<11; iBuf++) parFile >> buf;
    //mWFAmpBining >> mAmpMin >> mAmpMax not currently used in this code. See WaveformProcessor::processBaseline()
    parFile >> mRun >> mFileName >>  mWFAmpBining >> mAmpMin >> mAmpMax
            >> mNoise1Bin >> mRiseTimeSig >> mFallTimeTau >> mFallTimeTau2 >> mPolarity >> mMinChi2ForRefit;
    while(!parFile.eof() && mRun!=aRun){
        parFile >> mRun >> mFileName >>  mWFAmpBining >> mAmpMin >> mAmpMax
                >> mNoise1Bin >> mRiseTimeSig >> mFallTimeTau>> mFallTimeTau2 >> mPolarity >> mMinChi2ForRefit;
        std::cout << mRun << std::endl;
    }
    mDiffAmpThresh = 5*mNoise1Bin; // 5 times the noise baseline sigma should be sufficient to suppress bg efficient
    mMaxSPTemplateChi2 = 1e9;
    if(parFile.eof()){
        std::cerr << "Could not find run requested " << aRun << std::endl;
        exit(0);
    }
    // --- Open input file
    //>>> check file extension
    int iChar=strlen(mFileName)-1;
    while(iChar>0 && strncmp(mFileName+iChar,".",1)!=0) iChar--;
    if(strncmp(mFileName+iChar+1,"root",4)==0){
        mDataFile = new LecroyFile(mFileName);
    }
    else{
        if(strncmp(mFileName+iChar+1,"hdf5",4)==0){
            mDataFile = new LecroyHdfFile(mFileName);
        }
        else{
            std::cerr << "cannot tell file type. Aborting " << std::endl;
            exit(1);
        }
    }
    //if(quietMode == false) std::cout << "Waveform found : " << mDataFile->getWaveformCount() << std::endl;

    
    // --- set initial Pulse Ratio
    mPulseRatio = 0.9;

    // --- prepare vector of pulses
    mNFitPulseMax = numPulsesFit;
    mNPulse=0;
    mNPulseMax= mNFitPulseMax*2;
    mPulse = new Pulse[mNPulseMax];
    mPulseIndex = new int[mNPulseMax];
    mPulseTime = new double[mNPulseMax];

    // --- Function for pulse fitting
    mFExpGaus = (TF1*) gROOT->FindObjectAny("FExpGaus");
    if(mFExpGaus) mFExpGaus->Delete();

    // I do not think, that this line makes sense, why should one do this after 126
    mNFitPulseMax=mNPulseMax;
    mFExpGaus = new TF1("FExpGaus",FuncExpGausMulti,
                        0.,0.5e-6,4+mNFitPulseMax*2); // range reset later
    mFExpGaus->SetParLimits(1,0.,1e-6);// sigma
    mFExpGaus->SetParLimits(2,0.,1e-6);// tau
    strcpy(mFitOption,"QR0");

    mFExpGaus2 = (TF1*) gROOT->FindObjectAny("FExpGaus2");
    if(mFExpGaus2) mFExpGaus2->Delete();
    mFExpGaus2 = new TF1("FExpGaus2",FuncExpGausMulti2,
                         0.,0.5e-6,7+mNFitPulseMax*2); // range reset later
    mFExpGaus2->SetParLimits(1,0.,1e-6);// sigma
    mFExpGaus2->SetParLimits(2,0.,1e-6);// tau
    mFExpGaus2->SetParLimits(3,0.,1e-6);// tau2
}

// ---
WaveformProcessor* WaveformProcessor::mInstanceForRootMacro=0;
int WaveformProcessor::mRun=-1;
WaveformProcessor* WaveformProcessor::instanceForRootMacro(const char* aSetupFileName, int aRun,  int numPulsesFit){
    if(!mInstanceForRootMacro){ //!!!! Must remain a singleton. Force init if needed somewhere else
        mInstanceForRootMacro = new WaveformProcessor(aSetupFileName, aRun, numPulsesFit);
    }
    else{
        if(mRun!=aRun){
            mInstanceForRootMacro->clear();
            mInstanceForRootMacro->init(aSetupFileName, aRun, numPulsesFit);
        }
    }
    return mInstanceForRootMacro;
}

// ---
int WaveformProcessor::getWaveformCount(){
    return mDataFile->getWaveformCount();
}

//___________________________________________________________________
// ---
WaveformProcessor::~WaveformProcessor(){
    clear();
}
void WaveformProcessor::clear(){
    delete mDataFile;
    mHBaseline->Delete();
    mFExpGaus->Delete();
    mFExpGaus2->Delete();
    delete[] mPulse;
    delete[] mPulseIndex;
    delete[] mPulseTime;
}

//___________________________________________________________________
// --- Access the waveform
void WaveformProcessor::readNextWaveform(){
    mWF = mDataFile->getNextWaveform(); // index is not used!
    mWF->setQuietMode(quietMode);
}
void WaveformProcessor::readWaveform(int aIndex){
    mWF = mDataFile->getWaveform(aIndex);
    mWF->setQuietMode(quietMode);
}
void WaveformProcessor::readWaveform(const char* aHName){
    mWF = mDataFile->getWaveform(aHName);
    mWF->setQuietMode(quietMode);
}
int WaveformProcessor::getCurrentWaveformIndex(){
    char ciwf[50];
    strcpy(ciwf,mWF->GetName()+7);
    return atoi(ciwf);
}

//___________________________________________________________________
// --- baseline
int WaveformProcessor::processBaseline(){
    // --- Create histogram for baseline calculation
    mHBaseline = (TH1F*) gROOT->FindObjectAny("HBaseline");
    if(mHBaseline) mHBaseline->Delete();
    //mAmpMin = mWF->GetMinimum();
    //mAmpMax = mWF->GetMaximum();
    //int nBinsBaseline = 500; !!! Why using this?
    int nBinsBaseline = (mAmpMax-mAmpMin)/mWFAmpBining;
    mHBaseline = new TH1F("HBaseline","HBaseline",nBinsBaseline,mAmpMin,mAmpMax);
    mHBaseline->Reset("C");
    for(int iBin=1; iBin <= mWF->GetNbinsX(); iBin++){
        mHBaseline->Fill(mWF->GetBinContent(iBin));
    }
    int blMostProbBin=0;
    int blMostProbCont=0;
    for(int iBin=1; iBin <= mHBaseline->GetNbinsX(); iBin++){
        if(mHBaseline->GetBinContent(iBin) > blMostProbCont){
            blMostProbCont = mHBaseline->GetBinContent(iBin);
            blMostProbBin = iBin;
        }
    }
    mBmu=mHBaseline->GetBinCenter(blMostProbBin);
    mBRMS=mHBaseline->GetRMS();
    //std::cout << "The baseline mu is: " << mBmu << std::endl;
    //std::cout << "The number of baseline bins is: " << nBinsBaseline << std::endl;

    return mBRMS<5*mNoise1Bin; // Otherwise noisy. i.e. pick up
}

//___________________________________________________________________
// --- pulse finding
int WaveformProcessor::findPulse(){
    mPulseFound = false;
    int nBinRiseTime = round(mRiseTimeSig*3/mWF->GetBinWidth(1));
    int nBinFallTime = round(mFallTimeTau*3/mWF->GetBinWidth(1));

    mNPulse=0;
    for(int iBin=nBinRiseTime+1; iBin<=mWF->GetNbinsX(); iBin++){

        // first  condition: pulse is more than 4 sigma away from baseline
        // second condition: pulse is negative and has a certain height
        // third  condition: a width of at least 3 bins over threshold
        if((mPolarity*(mWF->GetBinContent(iBin)-mBmu)>3*mNoise1Bin) &&
                (mPolarity*(mWF->GetBinContent(iBin)-
                            mWF->GetBinContent(iBin-nBinRiseTime))>5*mNoise1Bin) )
        {
            mPulseFound = true;

            // >>> Find maximum amplitude
            double pulseAbsAmp=mPolarity*mWF->GetBinContent(iBin);
            iBin++;
            while(iBin <= mWF->GetNbinsX() &&
                  pulseAbsAmp <= mPolarity*(mWF->GetBinContent(iBin)-mBmu)){
                pulseAbsAmp=mPolarity*(mWF->GetBinContent(iBin)-mBmu);
                iBin++;
            }
            pulseAbsAmp+=(mBmu*mPolarity);
            pulseAbsAmp*=mPolarity;

            // >>> Calculate the baseline before the pulse
            double pulseBaseline=0.;
            for(int iBinBase=iBin-nBinRiseTime-12;
                iBinBase<(iBin-nBinRiseTime-2);
                iBinBase++){
                pulseBaseline+=mWF->GetBinContent(iBinBase);
            }
            pulseBaseline/=10.;
            double pulseAmp=(pulseAbsAmp-pulseBaseline)*mPolarity;

            // >>> Calculate the charge (for removing noise)
            double tCharge=0.;
            int iPulseStart=-1;
            int iPulseEnd=-1;
            double pulseWidth=-1.;
            for(int iBinInt=iBin-nBinRiseTime;
                iBinInt<iBin+nBinFallTime; iBinInt++){
                tCharge+=mWF->GetBinContent(iBinInt);
                tCharge-=mBmu;
                if(mPolarity*(mWF->GetBinContent(iBinInt)-pulseBaseline)/pulseAmp>0.25){
                    if(iPulseStart<0) iPulseStart=iBinInt;
                    iPulseEnd=iBinInt;
                }
                else{
                    if(iPulseStart>=0 && pulseWidth<0.) pulseWidth=iPulseEnd-iPulseStart;
                }
            }
            tCharge*=mPolarity;
            if(pulseWidth<0.) pulseWidth=iPulseEnd-iPulseStart;

            if(iBin < mWF->GetNbinsX() && // Do not keep pulses that are at the edge of the waveform
                    tCharge>3*sqrt(nBinRiseTime+nBinFallTime)*mNoise1Bin //&&
                    //pulseWidth>(nBinRiseTime+nBinFallTime)/2
                    ){
                iBin--;
                if(mNPulse<mNPulseMax){
                    mPulse[mNPulse].mTime=mWF->GetBinCenter(iBin);
                    mPulse[mNPulse].mAbsAmp=pulseAbsAmp;
                    mPulse[mNPulse].mAmp=pulseAmp;
                    mPulse[mNPulse].mBaseline=pulseBaseline;
                    mPulse[mNPulse].mQ=tCharge;
                    mPulse[mNPulse].mWidth=pulseWidth;
                    mPulseIndex[mNPulse]=mNPulse;
                    mNPulse++;
                }
            }
            iBin+=nBinFallTime;
        }
    }
    return mNPulse<mNPulseMax;
}
// ---
TGraph* WaveformProcessor::getPulseGraph(){
    double* tTime = new double[mNPulse];
    double* tAmp = new double[mNPulse];
    for(int iPulse=0; iPulse<mNPulse; iPulse++){
        tTime[iPulse]=mPulse[iPulse].mTime;
        tAmp[iPulse]=mPulse[iPulse].mAmp;
    }
    TGraph* tG = new TGraph(mNPulse,tTime,tAmp); //TGraph(number of points, xArray, yArray)
    delete[] tTime;
    delete[] tAmp;
    return tG;
}

//
// --- Single PE checker
void WaveformProcessor:: calcSinglePulseTemplateChi2(){
    // >>> set the function parameter
    mFExpGaus->SetParameter(0,mBmu);
    mFExpGaus->SetParameter(1,mRiseTimeSig);
    mFExpGaus->SetParameter(2,mFallTimeTau);
    mFExpGaus->SetParameter(3,1);
    for(int iPulse=1; iPulse<mNFitPulseMax; iPulse++){
        mFExpGaus->FixParameter(4+2*iPulse,0);
        mFExpGaus->FixParameter(5+2*iPulse,0);
    }
    for(int iPulse=0; iPulse<mNPulse; iPulse++){
        // >>>
        mFExpGaus->SetParameter(4,mPulse[iPulse].mAmp-mBmu);
        mFExpGaus->SetParameter(5,mPulse[iPulse].mTime);
        // >>>
        int iFirstBin = mWF->GetXaxis()->FindBin(mPulse[iPulse].mTime-mRiseTimeSig*5);
        if(iFirstBin<1) iFirstBin=1;
        int iLastBin = mWF->GetXaxis()->FindBin(mPulse[iPulse].mTime+mFallTimeTau*3);
        if(iLastBin>mWF->GetNbinsX()) iLastBin=mWF->GetNbinsX();

        // >>>
        mPulse[iPulse].mSPTemplateChi2=0;
        for(int iBin=iFirstBin; iBin<=iLastBin; iBin++){
            double val = (mWF->GetBinContent(iBin)-mFExpGaus->Eval(mWF->GetBinCenter(iBin)))/mNoise1Bin;
            mPulse[iPulse].mSPTemplateChi2+= (val*val);
        }
        mPulse[iPulse].mSPTemplateChi2/=(iLastBin-iFirstBin);
    }
}

//___________________________________________________________________
// --- fit Pulse
void WaveformProcessor::setFitOption(const char* aOption){
    strcpy(mFitOption,aOption);
}
void WaveformProcessor::fit(int aType){
    // aType = multiFit + 10* templateCheck . multiFit=2 for refiting 1 otherwise
    int tFitMulti = aType/100;
    int tTemplateCheck = (aType%100)/10;
    int tFitType = (aType%100)%10;
    //std::cout << "FIT TYPES: " << tFitMulti << " " << tTemplateCheck << " " << tFitType << std::endl;

    // >>> Template calculation (always do it for checking result)
    calcSinglePulseTemplateChi2();

    // >>> Set Histo error
    for(int iBin=1; iBin<=mWF->GetNbinsX(); iBin++){
        mWF->SetBinError(iBin,mNoise1Bin);
    }

    if(aType>0){
        int iPulse=0;
        while(iPulse<mNPulse){
            // --- Skip fit if template comparison was good
            if(tTemplateCheck && mPulse[iPulse].mSPTemplateChi2<mMaxSPTemplateChi2){
                mPulse[iPulse].mFitChi2 = mPulse[iPulse].mSPTemplateChi2;
                mPulse[iPulse].mFitNDF = -1;
                iPulse++;
            }
            else{

                // >>> Calculate fit limits
                // >>> Low limit given by first pulse
                mPulse[iPulse].mFitLowLimit = mPulse[iPulse].mTime-mRiseTimeSig*5-5*mWF->GetBinWidth(1); // fit range (low limit). Add 5 bins for baseline
                if(mPulse[iPulse].mFitLowLimit<mWF->GetXaxis()->GetXmin())
                    mPulse[iPulse].mFitLowLimit = mWF->GetXaxis()->GetXmin();
                // >>> Pulses may be merged in a group to be fitted together. The limit is adjusted accordingly
                int tNPulseInGroup=1; // total number of pulses fitted together
                mPulse[iPulse].mFitHighLimit =
                        mPulse[iPulse].mTime+mFallTimeTau*3+mRiseTimeSig*5+5*mWF->GetBinWidth(1);
                while(iPulse+tNPulseInGroup<mNPulse-1 &&
                      mPulse[iPulse].mFitHighLimit>mPulse[iPulse+tNPulseInGroup].mTime){
                    mPulse[iPulse].mFitHighLimit = mPulse[iPulse+tNPulseInGroup].mTime+mFallTimeTau*3+mRiseTimeSig*5+5*mWF->GetBinWidth(1);
                    tNPulseInGroup++;
                }
                if(mPulse[iPulse].mFitHighLimit>mWF->GetBinLowEdge(mWF->GetNbinsX()))
                    mPulse[iPulse].mFitHighLimit = mWF->GetBinLowEdge(mWF->GetNbinsX());
                for(int iFitPulse=iPulse; iFitPulse<iPulse+tNPulseInGroup; iFitPulse++){
                    mPulse[iFitPulse].mFirstPulseInGroup = iPulse;
                    mPulse[iFitPulse].mFitLowLimit=mPulse[iPulse].mFitLowLimit;
                    mPulse[iFitPulse].mFitHighLimit=mPulse[iPulse].mFitHighLimit;
                }

                // --- Set fit paremeters
                mFExpGaus->FixParameter(3,tNPulseInGroup);
                switch(tFitType){
                case 1: // fixed time constants but free baseline
                    mFExpGaus->ReleaseParameter(0); // testing: fix the baseline
                    mFExpGaus->FixParameter(0,mBmu);
                    mFExpGaus->FixParameter(1,mRiseTimeSig);
                    mFExpGaus->FixParameter(2,mFallTimeTau);
                    break;
                case 2: // free time constant but fixed baseline
                    mFExpGaus->FixParameter(0,mBmu);
                    mFExpGaus->ReleaseParameter(1);
                    mFExpGaus->SetParameter(1,mRiseTimeSig);
                    mFExpGaus->ReleaseParameter(2);
                    mFExpGaus->SetParameter(2,mFallTimeTau);
                    break;
                case 3: // free time constant but with tight limits
                    mFExpGaus->ReleaseParameter(0);
                    mFExpGaus->SetParameter(0,mBmu);
                    mFExpGaus->ReleaseParameter(1);
                    mFExpGaus->SetParameter(1,mRiseTimeSig);
                    mFExpGaus->SetParLimits(1,mRiseTimeSig*0.1, mRiseTimeSig*10);
                    mFExpGaus->ReleaseParameter(2);
                    mFExpGaus->SetParameter(2,mFallTimeTau);
                    mFExpGaus->SetParLimits(2,mFallTimeTau*0.1, mFallTimeTau*10);
                    break;
                }
                for(int iFitPulse=0; iFitPulse<tNPulseInGroup; iFitPulse++){
                    mFExpGaus->ReleaseParameter(4+2*iFitPulse);
                    mFExpGaus->SetParameter(4+2*iFitPulse,(mPulse[iFitPulse+iPulse].mAmp-mBmu));
                    mFExpGaus->ReleaseParameter(5+2*iFitPulse);
                    mFExpGaus->SetParameter(5+2*iFitPulse,mPulse[iFitPulse+iPulse].mTime);
                    mFExpGaus->SetParLimits(5+2*iFitPulse,mPulse[iPulse].mFitLowLimit,mPulse[iPulse].mFitHighLimit); // bad things happen when pulses are allowed in front.
                }
                for(int iFitPulse=tNPulseInGroup; iFitPulse<mNFitPulseMax; iFitPulse++){
                    mFExpGaus->FixParameter(4+2*iFitPulse,0);
                    mFExpGaus->FixParameter(5+2*iFitPulse,0);
                }
                mFExpGaus->SetRange(mPulse[iPulse].mFitLowLimit,mPulse[iPulse].mFitHighLimit);
                //std::cout << "fit limits: " << mPulse[iPulse].mFitLowLimit << "\t" << mPulse[iPulse].mFitHighLimit << std::endl;
                mWF->Fit("FExpGaus",mFitOption);

                //>>> Store the fit information
                for(int iFitPulse=0; iFitPulse<tNPulseInGroup; iFitPulse++){
                    mPulse[iFitPulse+iPulse].mFitAmp = mFExpGaus->GetParameter(4+2*iFitPulse);
                    mPulse[iFitPulse+iPulse].mFitTime = mFExpGaus->GetParameter(5+2*iFitPulse);
                    mPulse[iFitPulse+iPulse].mFitBaseline = mFExpGaus->GetParameter(0);
                    mPulse[iFitPulse+iPulse].mFitRiseTime = mFExpGaus->GetParameter(1);
                    mPulse[iFitPulse+iPulse].mFitFallTime = mFExpGaus->GetParameter(2);
                    mPulse[iFitPulse+iPulse].mFitChi2 = mFExpGaus->GetChisquare();
                    mPulse[iFitPulse+iPulse].mFitNDF = mFExpGaus->GetNDF();
                    mPulse[iFitPulse+iPulse].mRefitChi2 = -1;
                    mPulse[iFitPulse+iPulse].mRefitNDF = -1;
                    mPulse[iFitPulse+iPulse].mFitFallTime2 = -1;
                    //std::cout<< "Fit reduced chi2:"<< mFExpGaus->GetChisquare()/mFExpGaus->GetNDF() << std::endl;
                }
                iPulse+=tNPulseInGroup;
            }
        }
    }
    if(tFitMulti) reFitMulti();
    // sorting of pulses should happen here....

    // >>> Sort
    if(mNPulse>1){
        for(int iPulse=0; iPulse<mNPulse; iPulse++){
            mPulseIndex[iPulse]=iPulse;
            mPulseTime[iPulse]=mPulse[iPulse].mFitTime;
        }
        TMath::Sort(mNPulse, mPulseTime, mPulseIndex, false); // root sorts from high to low per default
    }
}



// ---
void WaveformProcessor::reFitMulti(){
    //    std::cout << "WaveformProcessor::reFitMulti  " << mNPulse<< std::endl;
    int iPulse=0;
    while(iPulse<mNPulse){
        if(mPulse[iPulse].mFirstPulseInGroup==iPulse &&
                mPulse[iPulse].mFitNDF>0 &&
                (mPulse[iPulse].mFitChi2/mPulse[iPulse].mFitNDF)>mMinChi2ForRefit){ // refit this group

            //            std::cout << "Refitting " << iPulse << " "
            //                      << mPulse[iPulse].mFitLowLimit << " " << mPulse[iPulse].mFitHighLimit << std::endl;

            // >>>
            mFExpGaus->SetRange(mPulse[iPulse].mFitLowLimit, mPulse[iPulse].mFitHighLimit);
            mFExpGaus->ReleaseParameter(0);
            mFExpGaus->SetParameter(0,mBmu);
            mFExpGaus->FixParameter(1,mRiseTimeSig);
            mFExpGaus->FixParameter(2,mFallTimeTau);
            mPulse[iPulse].mRefitChi2 = mPulse[iPulse].mFitChi2;
            mPulse[iPulse].mRefitNDF = mPulse[iPulse].mFitNDF;

            double tMinTimeDiff=1.; // minimum time between two pulses
            int tNFitPulse=1;
            while(tNFitPulse<(mNFitPulseMax-1) && //can add one more pulse due to n pulse fit limit
                  mNPulse<(mNPulseMax-1) &&  //can add one more pulse due to total n pulse limit
                  mPulse[iPulse].mRefitNDF>0 &&
                  (mPulse[iPulse].mRefitChi2/mPulse[iPulse].mRefitNDF)>mMinChi2ForRefit &&
                  tMinTimeDiff>2e-9)
            {

                // >>> Set fit function starting parameters without additional pulse
                int iFitParameter=0;
                for(int iFitPulse=iPulse; iFitPulse<mNPulse; iFitPulse++){
                    if(mPulse[iFitPulse].mFirstPulseInGroup==iPulse){
                        mFExpGaus->ReleaseParameter(4+2*iFitParameter);
                        // numeric issue in root: If the FitParameter is exactly at the Range edge
                        // it will create an fit error and it should never be larger
                        if(mPulse[iFitPulse].mFitAmp < mNoise1Bin &&mPulse[iFitPulse].mFitAmp > -10)
                            mFExpGaus->SetParameter(4+2*iFitParameter,mPulse[iFitPulse].mFitAmp);
                        else if(mPulse[iFitPulse].mFitAmp == mNoise1Bin)
                            mFExpGaus->SetParameter(4+2*iFitParameter,mPulse[iFitPulse].mFitAmp-mNoise1Bin/100000.);
                        else if(mPulse[iFitPulse].mFitAmp == mNoise1Bin)
                            mFExpGaus->SetParameter(4+2*iFitParameter,mPulse[iFitPulse].mFitAmp+mNoise1Bin/100000.);

                        //    mFExpGaus->SetParameter(4+2*iFitParameter,mPulse[iFitPulse].mFitAmp-mNoise1Bin/100000.);
                        mFExpGaus->SetParLimits(4+2*iFitParameter,-10.,mNoise1Bin);
                        mFExpGaus->ReleaseParameter(5+2*iFitParameter);
                        mFExpGaus->SetParameter(5+2*iFitParameter,mPulse[iFitPulse].mFitTime);
                        mFExpGaus->SetParLimits(5+2*iFitParameter,mPulse[iPulse].mFitLowLimit,mPulse[iPulse].mFitHighLimit);
                        iFitParameter++;

                        //                        if(mPulse[iFitPulse].mFitAmp-mNoise1Bin/100000. > mNoise1Bin || mPulse[iFitPulse].mFitAmp < -10.)
                        //                            std::cout << "Amp: " <<mPulse[iFitPulse].mFitAmp << " "<< mNoise1Bin <<std::endl;
                        //                        if(mPulse[iFitPulse].mFitTime > mPulse[iPulse].mFitHighLimit || mPulse[iFitPulse].mFitTime < mPulse[iPulse].mFitLowLimit)
                        //                            std::cout << "time" << std::endl;

                    }
                }
                // baseline set to fixed value - will increase the speed and makes sure, that the right baseline is used.
                mFExpGaus->FixParameter(0,mBmu);
                //std::cout<<"MuBaseline Parameter " << mBmu<<std::endl;
                mFExpGaus->FixParameter(3,tNFitPulse);

                // >>> Look for most likely position of next pulse
                int tLastBin = mWF->GetXaxis()->FindBin(mPulse[iPulse].mFitHighLimit)-1;
                int iMaxDiff;
                double maxDiff=0.;
                double newPulseTime=0.;
                double newPulseAmp=0;
                double tDiff;
                for(int iBin=mWF->GetXaxis()->FindBin(mPulse[iPulse].mFitLowLimit)+1;
                    iBin<=tLastBin; iBin++){
                    tDiff=mPolarity*(mWF->GetBinContent(iBin)-
                                     mFExpGaus->Eval(mWF->GetBinCenter(iBin)));
                    if(maxDiff<tDiff){
                        maxDiff=tDiff;
                        newPulseTime =  mWF->GetBinCenter(iBin);
                        newPulseAmp = mWF->GetBinContent(iBin)-mBmu;
                    }
                }

                // >>> Abort if too close to an existing pulse
                tMinTimeDiff=1.;
                for(int iFitPulse=0; iFitPulse<mNPulse; iFitPulse++){
                    if(mPulse[iFitPulse].mFirstPulseInGroup==iPulse){
                        tDiff=fabs(mPulse[iFitPulse].mFitTime-newPulseTime);
                        if(tMinTimeDiff>tDiff) tMinTimeDiff=tDiff;
                    }
                }
                if(tMinTimeDiff>2e-9){
                    // >>> Add one more pulse
                    mFExpGaus->ReleaseParameter(4+2*tNFitPulse);
                    mFExpGaus->SetParameter(4+2*tNFitPulse,newPulseAmp);
                    mFExpGaus->SetParLimits(4+2*tNFitPulse,-10.,mNoise1Bin);
                    mFExpGaus->ReleaseParameter(5+2*tNFitPulse);
                    mFExpGaus->SetParameter(5+2*tNFitPulse,newPulseTime);
                    mFExpGaus->SetParLimits(5+2*tNFitPulse,mPulse[iPulse].mFitLowLimit,mPulse[iPulse].mFitHighLimit);
                    tNFitPulse++;
                    mFExpGaus->FixParameter(3,tNFitPulse);
                    mPulse[mNPulse].mFirstPulseInGroup=iPulse;
                    mNPulse++;
                    // debug mode -> find the source of the error:
                    //Error in ROOT::Math::ParameterSettings>: Invalid lower/upper bounds - ignoring the bounds
                    if(newPulseTime <   mPulse[iPulse].mFitLowLimit|| newPulseTime > mPulse[iPulse].mFitHighLimit)
                    {
                        std::cout << "new t: " << newPulseTime<< "   " << mPulse[iPulse].mFitLowLimit
                                  <<"  " << mPulse[iPulse].mFitHighLimit<< std::endl;
                    }
                    if(newPulseAmp > mNoise1Bin || newPulseAmp < (-10.0) )
                    {
                        std::cout << "new a: " << newPulseAmp<< "   " << mNoise1Bin <<std::endl;
                    }
                    // >>> Fit
                    mWF->Fit("FExpGaus",mFitOption);



                    // >>> Copy fit information
                    iFitParameter=0;
                    for(int iFitPulse=iPulse; iFitPulse<mNPulse; iFitPulse++){
                        if(mPulse[iFitPulse].mFirstPulseInGroup==iPulse){
                            mPulse[iFitPulse].mFitAmp = mFExpGaus->GetParameter(4+2*iFitParameter);
                            mPulse[iFitPulse].mFitTime = mFExpGaus->GetParameter(5+2*iFitParameter);
                            mPulse[iFitPulse].mFitBaseline = mFExpGaus->GetParameter(0);
                            mPulse[iFitPulse].mFitRiseTime = mFExpGaus->GetParameter(1);
                            mPulse[iFitPulse].mFitFallTime = mFExpGaus->GetParameter(2);
                            mPulse[iFitPulse+iPulse].mFitFallTime2 = -1;
                            mPulse[iFitPulse].mRefitChi2 = mFExpGaus->GetChisquare();
                            mPulse[iFitPulse].mRefitNDF = mFExpGaus->GetNDF();
                            //std::cout << "Refit: " << mFExpGaus->GetChisquare()/mFExpGaus->GetNDF() << std::endl;
                            iFitParameter++;
                        }
                    }
                }
            }
        }
        iPulse++;
    }
    // previous sorting was here -> moved to end of fitting

}


/**
 *
 *  start double peak fits
 *
 *
 *
 *
 */

void WaveformProcessor::fit2(int aType,bool _2TC)
{

    //std::cout << "---------------------------2 TC FIT!!!!"<<std::endl;
    // Temporary 2nd fall time to test fitting:

    // Use or ignore 2nd TC
    if(_2TC)
    {
        mFExpGaus2->FixParameter(5,1);
        mFExpGaus2->ReleaseParameter(4);
        mFExpGaus2->SetParameter(4,mPulseRatio);

    }
    if(!_2TC)
    {
        mPulseRatio = 0;
        mFExpGaus2->FixParameter(5,0);
        mFExpGaus2->SetParameter(4,mPulseRatio);
        // set 2nd TC to 0, if not used
        mFallTimeTau2 = 0;
    }
    // aType = multiFit + 10* templateCheck . multiFit=2 for refiting 1 otherwise
    int tFitMulti = aType/100;
    int tTemplateCheck = (aType%100)/10;
    int tFitType = (aType%100)%10;
    //std::cout << "FIT TYPES: " << tFitMulti << " " << tTemplateCheck << " " << tFitType << std::endl;

    // >>> Template calculation (always do it for checking result)
    //calcSinglePulseTemplateChi2();

    // >>> Set Histo error
    for(int iBin=1; iBin<=mWF->GetNbinsX(); iBin++){
        mWF->SetBinError(iBin,mNoise1Bin);
    }

    if(aType>0){
        int iPulse=0;
        while(iPulse<mNPulse){
            // --- Skip fit if template comparison was good
            if(tTemplateCheck && mPulse[iPulse].mSPTemplateChi2<mMaxSPTemplateChi2){
                mPulse[iPulse].mFitChi2 = mPulse[iPulse].mSPTemplateChi2;
                mPulse[iPulse].mFitNDF = -1;
                iPulse++;
            }
            else{

                // >>> Calculate fit limits
                // >>> Low limit given by first pulse
                mPulse[iPulse].mFitLowLimit = mPulse[iPulse].mTime-mRiseTimeSig*5-5*mWF->GetBinWidth(1); // fit range (low limit). Add 5 bins for baseline
                if(mPulse[iPulse].mFitLowLimit<mWF->GetXaxis()->GetXmin())
                    mPulse[iPulse].mFitLowLimit = mWF->GetXaxis()->GetXmin();

                // >>> Pulses may be merged in a group to be fitted together. The high limit is adjusted accordingly
                int tNPulseInGroup=1; // total number of pulses fitted together
                mPulse[iPulse].mFitHighLimit =
                        mPulse[iPulse].mTime+mFallTimeTau*3+mFallTimeTau2+mRiseTimeSig*5+5*mWF->GetBinWidth(1); // fit range includes one long TC

                while(iPulse+tNPulseInGroup<mNPulse &&                  // -1 removed
                      mPulse[iPulse].mFitHighLimit>mPulse[iPulse+tNPulseInGroup].mTime)
                {
                    mPulse[iPulse].mFitHighLimit = mPulse[iPulse+tNPulseInGroup].mTime // time of last pulse in group
                                                 +mFallTimeTau*3+mFallTimeTau2       // + falltimes
                                                 +mRiseTimeSig*5                     // + risetime -> should not play any role?
                                                 +5*mWF->GetBinWidth(1);             // + baseline
                    tNPulseInGroup++;
                }
                if(mPulse[iPulse].mFitHighLimit>mWF->GetBinLowEdge(mWF->GetNbinsX()))
                    mPulse[iPulse].mFitHighLimit = mWF->GetBinLowEdge(mWF->GetNbinsX());
                for(int iFitPulse=iPulse; iFitPulse<iPulse+tNPulseInGroup; iFitPulse++){
                    mPulse[iFitPulse].mFirstPulseInGroup = iPulse;
                    mPulse[iFitPulse].mFitLowLimit=mPulse[iPulse].mFitLowLimit;
                    mPulse[iFitPulse].mFitHighLimit=mPulse[iPulse].mFitHighLimit;
                }

                // --- Set fit paremeters
                mFExpGaus2->FixParameter(6,tNPulseInGroup);
                switch(tFitType){
                case 1: // fixed time constants but free baseline & free long tc & fixed PR
                    mFExpGaus2->ReleaseParameter(0); // testing: fix the baseline
                    mFExpGaus2->FixParameter(0,mBmu);
                    mFExpGaus2->FixParameter(1,mRiseTimeSig);
                    mFExpGaus2->FixParameter(2,mFallTimeTau);
                    mFExpGaus2->FixParameter(3,mFallTimeTau2);
                    mFExpGaus2->FixParameter(4, mPulseRatio);
		    //std::cout << "Case 1" << std:: endl;
                    break;
                case 2: // free time constant but fixed baseline, 2nd TC = 0
                    mFExpGaus2->FixParameter(0,mBmu);
                    mFExpGaus2->ReleaseParameter(1);
                    mFExpGaus2->SetParameter(1,mRiseTimeSig);
                    mFExpGaus2->SetParLimits(1,mRiseTimeSig*0.1, mRiseTimeSig*10);
                    mFExpGaus2->ReleaseParameter(2);
                    mFExpGaus2->SetParameter(2,mFallTimeTau);
                    mFExpGaus2->SetParLimits(2,mFallTimeTau*0.1, mFallTimeTau*10);
                    mFExpGaus2->FixParameter(5,0);
                    mFExpGaus2->FixParameter(4,1);
                    //std::cout << "Case 2" << std:: endl;
                    break;
                case 3: // free time constant but with tight limits and free PR
                    mFExpGaus2->ReleaseParameter(0);
                    mFExpGaus2->FixParameter(0,mBmu);
                    mFExpGaus2->ReleaseParameter(1);
                    mFExpGaus2->SetParameter(1,mRiseTimeSig);
                    mFExpGaus2->SetParLimits(1,mRiseTimeSig*0.1, mRiseTimeSig*10);
                    mFExpGaus2->ReleaseParameter(2);
                    mFExpGaus2->SetParameter(2,mFallTimeTau);
                    mFExpGaus2->SetParLimits(2,mFallTimeTau*0.1, mFallTimeTau*10);
                    //mFExpGaus2->FixParameter(1,mRiseTimeSig);
                    //mFExpGaus2->FixParameter(2,mFallTimeTau);

                    mFExpGaus2->SetParameter(3,mFallTimeTau2);
                    mFExpGaus2->SetParLimits(3,mFallTimeTau2*0.1, mFallTimeTau2*1);
                    mFExpGaus2->ReleaseParameter(4);
                    mFExpGaus2->SetParameter(4,0.9);
                    //std::cout << "Case 3" << std:: endl;
                    break;
                }
                for(int iFitPulse=0; iFitPulse<tNPulseInGroup; iFitPulse++){
                    mFExpGaus2->ReleaseParameter(7+2*iFitPulse);
                    mFExpGaus2->SetParameter(7+2*iFitPulse,(mPulse[iFitPulse+iPulse].mAmp-mBmu));
                    mFExpGaus2->ReleaseParameter(8+2*iFitPulse);
                    mFExpGaus2->SetParameter(8+2*iFitPulse,mPulse[iFitPulse+iPulse].mTime);
                    mFExpGaus2->SetParLimits(8+2*iFitPulse,mPulse[iPulse].mFitLowLimit,mPulse[iPulse].mFitHighLimit); // bad things happen when pulses are allowed in front.
                }
                for(int iFitPulse=tNPulseInGroup; iFitPulse<mNFitPulseMax; iFitPulse++){
                    mFExpGaus2->FixParameter(7+2*iFitPulse,0);
                    mFExpGaus2->FixParameter(8+2*iFitPulse,0);
                }
                //set the actual fit range - must be adjusted according the use of the TCs
                mFExpGaus2->SetRange(mPulse[iPulse].mFitLowLimit,mPulse[iPulse].mFitHighLimit);

                mWF->Fit("FExpGaus2",mFitOption);

                //>>> Store the fit information
                for(int iFitPulse=0; iFitPulse<tNPulseInGroup; iFitPulse++){
                    mPulse[iFitPulse+iPulse].mFitAmp = mFExpGaus2->GetParameter(7+2*iFitPulse);
                    mPulse[iFitPulse+iPulse].mFitTime = mFExpGaus2->GetParameter(8+2*iFitPulse);
                    mPulse[iFitPulse+iPulse].mFitBaseline = mFExpGaus2->GetParameter(0);
                    mPulse[iFitPulse+iPulse].mFitRiseTime = mFExpGaus2->GetParameter(1);
                    mPulse[iFitPulse+iPulse].mFitFallTime = mFExpGaus2->GetParameter(2);
                    mPulse[iFitPulse+iPulse].mFitFallTime2 = mFExpGaus2->GetParameter(3);
                    mPulse[iFitPulse].mFitPulseRatio = mFExpGaus2->GetParameter(4);
                    mPulse[iFitPulse+iPulse].mFitChi2 = mFExpGaus2->GetChisquare();
                    mPulse[iFitPulse+iPulse].mFitNDF = mFExpGaus2->GetNDF();
                    mPulse[iFitPulse+iPulse].mRefitChi2 = -1;
                    mPulse[iFitPulse+iPulse].mRefitNDF = -1;
                    //std::cout<< "FFT2: "<< mFExpGaus2->GetParameter(3) << std::endl;
                }
                iPulse+=tNPulseInGroup;
            }
        }
    }

    if(tFitMulti) reFitMulti2();
    // sorting of pulses should happen here....

    // >>> Sort
    if(mNPulse>1){
        for(int iPulse=0; iPulse<mNPulse; iPulse++){
            mPulseIndex[iPulse]=iPulse;
            mPulseTime[iPulse]=mPulse[iPulse].mFitTime;
        }
        TMath::Sort(mNPulse, mPulseTime, mPulseIndex, false); // root sorts from high to low per default
    }
}



// ---
void WaveformProcessor::reFitMulti2(){
    //std::cout << "WaveformProcessor::reFitMulti  " << mNPulse<< std::endl;
    int iPulse=0;
    while(iPulse<mNPulse){
        if(mPulse[iPulse].mFirstPulseInGroup==iPulse &&
                mPulse[iPulse].mFitNDF>0 &&
                (mPulse[iPulse].mFitChi2/mPulse[iPulse].mFitNDF)>mMinChi2ForRefit){ // refit this group

            //            std::cout << "Refitting " << iPulse << " "
            //                      << mPulse[iPulse].mFitLowLimit << " " << mPulse[iPulse].mFitHighLimit << std::endl;

            // >>>
            mFExpGaus2->SetRange(mPulse[iPulse].mFitLowLimit, mPulse[iPulse].mFitHighLimit);
            mFExpGaus2->ReleaseParameter(0);
            mFExpGaus2->SetParameter(0,mBmu);
            mFExpGaus2->FixParameter(1,mRiseTimeSig);
            mFExpGaus2->FixParameter(2,mFallTimeTau);
            mFExpGaus2->FixParameter(3,mFallTimeTau2);
            mFExpGaus2->FixParameter(4,mPulseRatio);
	    
	    mPulse[iPulse].mRefitChi2 = mPulse[iPulse].mFitChi2;
            mPulse[iPulse].mRefitNDF = mPulse[iPulse].mFitNDF;

            double tMinTimeDiff=1.; // minimum time between two pulses
            int tNFitPulse=1;
            while(tNFitPulse<(mNFitPulseMax-1) && //can add one more pulse due to n pulse fit limit
                  mNPulse<(mNPulseMax-1) &&  //can add one more pulse due to total n pulse limit
                  mPulse[iPulse].mRefitNDF>0 &&
                  (mPulse[iPulse].mRefitChi2/mPulse[iPulse].mRefitNDF)>mMinChi2ForRefit &&
                  tMinTimeDiff>2e-9)
            {

                // >>> Set fit function starting parameters without additional pulse
                int iFitParameter=0;
                for(int iFitPulse=iPulse; iFitPulse<mNPulse; iFitPulse++){
                    if(mPulse[iFitPulse].mFirstPulseInGroup==iPulse){
                        mFExpGaus2->ReleaseParameter(7+2*iFitParameter);
                        // numeric issue in root: If the FitParameter is exactly at the Range edge
                        // it will create an fit error and it should never be larger
                        if(mPulse[iFitPulse].mFitAmp < mNoise1Bin &&mPulse[iFitPulse].mFitAmp > -10)
                            mFExpGaus2->SetParameter(7+2*iFitParameter,mPulse[iFitPulse].mFitAmp);
                        else if(mPulse[iFitPulse].mFitAmp == mNoise1Bin)
                            mFExpGaus2->SetParameter(7+2*iFitParameter,mPulse[iFitPulse].mFitAmp-mNoise1Bin/100000.);
                        else if(mPulse[iFitPulse].mFitAmp == mNoise1Bin)
                            mFExpGaus2->SetParameter(7+2*iFitParameter,mPulse[iFitPulse].mFitAmp+mNoise1Bin/100000.);

                        //    mFExpGaus2->SetParameter(4+2*iFitParameter,mPulse[iFitPulse].mFitAmp-mNoise1Bin/100000.);
                        mFExpGaus2->SetParLimits(7+2*iFitParameter,-10.,mNoise1Bin);
                        mFExpGaus2->ReleaseParameter(8+2*iFitParameter);
                        mFExpGaus2->SetParameter(8+2*iFitParameter,mPulse[iFitPulse].mFitTime);
                        mFExpGaus2->SetParLimits(8+2*iFitParameter,mPulse[iPulse].mFitLowLimit,mPulse[iPulse].mFitHighLimit);
                        iFitParameter++;

                    }
                }
                // baseline set to fixed value - will increase the speed and makes sure, that the right baseline is used.
                mFExpGaus2->FixParameter(0,mBmu);
                //std::cout<<"MuBaseline Parameter " << mBmu<<std::endl;
                mFExpGaus2->FixParameter(6,tNFitPulse);

                // >>> Look for most likely position of next pulse
                int tLastBin = mWF->GetXaxis()->FindBin(mPulse[iPulse].mFitHighLimit)-1;
                int iMaxDiff;
                double maxDiff=0.;
                double newPulseTime=0.;
                double newPulseAmp=0;
                double tDiff;
                for(int iBin=mWF->GetXaxis()->FindBin(mPulse[iPulse].mFitLowLimit)+1;
                    iBin<=tLastBin; iBin++){
                    tDiff=mPolarity*(mWF->GetBinContent(iBin)-
                                     mFExpGaus2->Eval(mWF->GetBinCenter(iBin)));
                    if(maxDiff<tDiff){
                        maxDiff=tDiff;
                        newPulseTime =  mWF->GetBinCenter(iBin);
                        newPulseAmp = mWF->GetBinContent(iBin)-mBmu;
                    }
                }

                // >>> Abort if too close to an existing pulse
                tMinTimeDiff=1.;
                for(int iFitPulse=0; iFitPulse<mNPulse; iFitPulse++){
                    if(mPulse[iFitPulse].mFirstPulseInGroup==iPulse){
                        tDiff=fabs(mPulse[iFitPulse].mFitTime-newPulseTime);
                        if(tMinTimeDiff>tDiff) tMinTimeDiff=tDiff;
                    }
                }
                if(tMinTimeDiff>2e-9){
                    // >>> Add one more pulse
                    mFExpGaus2->ReleaseParameter(7+2*tNFitPulse);
                    mFExpGaus2->SetParameter(7+2*tNFitPulse,newPulseAmp);
                    mFExpGaus2->SetParLimits(7+2*tNFitPulse,-10.,mNoise1Bin);
                    mFExpGaus2->ReleaseParameter(8+2*tNFitPulse);
                    mFExpGaus2->SetParameter(8+2*tNFitPulse,newPulseTime);
                    mFExpGaus2->SetParLimits(8+2*tNFitPulse,mPulse[iPulse].mFitLowLimit,mPulse[iPulse].mFitHighLimit);
                    tNFitPulse++;
                    mFExpGaus2->FixParameter(6,tNFitPulse);
                    mPulse[mNPulse].mFirstPulseInGroup=iPulse;
                    mNPulse++;
                    // debug mode -> find the source of the error:
                    //Error in ROOT::Math::ParameterSettings>: Invalid lower/upper bounds - ignoring the bounds
                    if(newPulseTime <   mPulse[iPulse].mFitLowLimit|| newPulseTime > mPulse[iPulse].mFitHighLimit)
                    {
                        std::cout << "new t: " << newPulseTime<< "   " << mPulse[iPulse].mFitLowLimit
                                  <<"  " << mPulse[iPulse].mFitHighLimit<< std::endl;
                    }
                    if(newPulseAmp > mNoise1Bin || newPulseAmp < (-10.0) )
                    {
                        std::cout << "new a: " << newPulseAmp<< "   " << mNoise1Bin <<std::endl;
                    }
                    // >>> Fit
                    mWF->Fit("FExpGaus2",mFitOption);



                    // >>> Copy fit information
                    iFitParameter=0;
                    for(int iFitPulse=iPulse; iFitPulse<mNPulse; iFitPulse++){
                        if(mPulse[iFitPulse].mFirstPulseInGroup==iPulse){
                            mPulse[iFitPulse].mFitAmp = mFExpGaus2->GetParameter(7+2*iFitParameter);
                            mPulse[iFitPulse].mFitTime = mFExpGaus2->GetParameter(8+2*iFitParameter);
                            mPulse[iFitPulse].mFitBaseline = mFExpGaus2->GetParameter(0);
                            mPulse[iFitPulse].mFitRiseTime = mFExpGaus2->GetParameter(1);
                            mPulse[iFitPulse].mFitFallTime = mFExpGaus2->GetParameter(2);
                            mPulse[iFitPulse].mFitPulseRatio = mFExpGaus2->GetParameter(4);
                            mPulse[iFitPulse+iPulse].mFitFallTime2 = mFExpGaus2->GetParameter(3);
                            mPulse[iFitPulse].mFitChi2 = mPulse[iPulse].mFitChi2;
                            mPulse[iFitPulse].mFitNDF = mPulse[iPulse].mFitNDF;
                            mPulse[iFitPulse].mRefitChi2 = mFExpGaus2->GetChisquare();
                            mPulse[iFitPulse].mRefitNDF = mFExpGaus2->GetNDF();
                            //std::cout << "Refit: " << mFExpGaus2->GetChisquare()/mFExpGaus2->GetNDF() << std::endl;
                            iFitParameter++;
                        }
                    }
                }
            }
        }
        iPulse++;
    }
}



/**
 *
 *
 * end double peak fits
 *
 *
 */
double WaveformProcessor::getTriggerTime(){
    return mDataFile->getTriggerTime();
}

TF1* WaveformProcessor::getFitFunction() {
    // include all pulses and full range of the waveform
    mFExpGaus->SetLineColor(2);
    mFExpGaus->SetRange(mWF->GetXaxis()->GetXmin(),
                        mWF->GetXaxis()->GetXmax());
    int tNFitPulse;
    if(mNPulse>mNFitPulseMax){
        std::cout << "WARNING, number of pulses exceed max allowed for function " << mNPulse << " vs "
                  << mNFitPulseMax << " allowed" << std::endl;
        tNFitPulse = mNFitPulseMax;
    }
    else{
        tNFitPulse = mNPulse;
    }
    mFExpGaus->SetParameter(3,tNFitPulse);

    for(int iPulse=0; iPulse<tNFitPulse; iPulse++){
        mFExpGaus->SetParameter(4+2*iPulse,mPulse[iPulse].mFitAmp);
        mFExpGaus->SetParameter(5+2*iPulse,mPulse[iPulse].mFitTime);
        //        std::cout<< "getFitFunction: " << mPulseIndex[iPulse] << " "
        //                 << mPulse[mPulseIndex[iPulse]].mFitTime << " " << mPulse[mPulseIndex[iPulse]].mFitAmp << std::endl;
    }
    for(int iPulse=tNFitPulse; iPulse<mNFitPulseMax; iPulse++){
        mFExpGaus->FixParameter(4+2*iPulse,0);
        mFExpGaus->FixParameter(5+2*iPulse,0);
    }
    return mFExpGaus;
}
TF1* WaveformProcessor::getFitFunction2() {
    // include all pulses and full range of the waveform
    mFExpGaus2->SetLineColor(7);
    mFExpGaus2->SetRange(mWF->GetXaxis()->GetXmin(),
                         mWF->GetXaxis()->GetXmax());
    int tNFitPulse;
    if(mNPulse>mNFitPulseMax){
        std::cout << "WARNING, number of pulses exceed max allowed for function " << mNPulse << " vs "
                  << mNFitPulseMax << " allowed" << std::endl;
        tNFitPulse = mNFitPulseMax;
    }
    else{
        tNFitPulse = mNPulse;
    }
    mFExpGaus2->SetParameter(6,tNFitPulse);

    for(int iPulse=0; iPulse<tNFitPulse; iPulse++){
        mFExpGaus2->SetParameter(7+2*iPulse,mPulse[iPulse].mFitAmp);
        mFExpGaus2->SetParameter(8+2*iPulse,mPulse[iPulse].mFitTime);
    }
    for(int iPulse=tNFitPulse; iPulse<mNFitPulseMax; iPulse++){
        mFExpGaus2->FixParameter(7+2*iPulse,0);
        mFExpGaus2->FixParameter(8+2*iPulse,0);
    }
    return mFExpGaus2;
}
TH1F* WaveformProcessor::getPulseHisto(){
    return pulseHisto;
}
void WaveformProcessor::setQuietMode(bool mode){
    quietMode = mode;
}
double WaveformProcessor::checkNoise()
{
    //std::cout << mWF->getMax() << "\t" << mWF->getMeanBaseline() << std::endl;

    return mWF->getMax();

}



