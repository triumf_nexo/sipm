    //g++ hdf5toRoot.cxx -o hdf5toRoot.exe -I/home/deap/packages/root/include -L/home/deap/packages/root/lib -lCore -lCint -lRIO -lNet -lHist -lGraf -lGraf3d -lGpad -lTree -lRint -lPostscript -lMatrix -lPhysics -lMathCore -lThread -pthread -lm -ldl -rdynamic

//  Giuseppe Vacanti (cosine science & computing bv)
    //  April 23, 2004
    //
    //   $Id: .emacs,v 1.9 2004/04/17 18:42:11 gvacanti Exp $

    #include <string>

    #include <H5Cpp.h>
    #include <iostream>
    #include <cassert>
    #include <vector>

    #include "TROOT.h"
    #include "TFile.h"
    #include "TNtuple.h"

    namespace {

      char map_h5type_to_root(H5::DataType type) {
       
        if(type == H5::PredType::NATIVE_SHORT){
          return 'B';
        }
        if(type == H5::PredType::NATIVE_USHORT){
          return 'b';
        }
        if(type == H5::PredType::NATIVE_INT){
          if(type.getSize() == 16)
       return 'S';
          else
       return 'I';
        }
        if(type == H5::PredType::NATIVE_UINT){
          if(type.getSize() == 16)
       return 's';
          else
       return 'I';
        }
        if(type == H5::PredType::NATIVE_LONG){
          return 'I';
        }
        if(type == H5::PredType::NATIVE_ULONG){
          return 'i';
        }
        if(type == H5::PredType::NATIVE_FLOAT){
          return 'F';
        }
        if(type == H5::PredType::NATIVE_DOUBLE){
          return 'D';
        }
       
        bool h5_predtype_not_known = false;
        assert(h5_predtype_not_known);
      }
    };


    int main(int argc, char * argv[]) {
     
      using namespace std;
      using namespace H5;

      // h5toroot <hdffile> <hdf5 dataset name> <root file name>
      if(argc != 4){
        cout << "Usage: " << argv[0] << " <hdffile> <hdf5 dataset name> <root file name>\n";
        exit(1);
      }


      const string tablename(argv[2]);
      const string filename(argv[1]);
      const string rootfile(argv[3]);
       

      H5File h5 = H5File(filename, H5F_ACC_RDONLY);
      Group root = h5.openGroup("/");
      DataSet ds = root.openDataSet(tablename);

      DataSpace dsp = ds.getSpace();
      cout << "Found the table " << tablename << "\n";
      cout << "Rank: " << dsp.getSimpleExtentNdims() << "\n";
      if(dsp.getSimpleExtentNdims() != 1){
        cout << "Cannot handle tables with rank != 1.";
        exit(1);
      }

      const hssize_t nrecs = dsp.getSimpleExtentNpoints();
      const CompType type = ds.getCompType();
      const int nm = type.getNmembers();
      const size_t twidth = type.getSize();

      TFile * rfile = new TFile(rootfile.c_str(),
                 "RECREATE",
                 "Dump of HDF5 file");
      TTree * rtree = new TTree("table", "table");
     

      string description;

      vector<size_t> offsets(nm);
      vector<char> rflags(nm);
      for(int k = 0; k < nm; ++k) {
        offsets[k] = type.getMemberOffset(k);
        rflags[k] = map_h5type_to_root(type.getMemberDataType(k));
        description += type.getMemberName(k) + "/" + rflags[k] + ":";     
      }
      description.erase(description.size() - 1);
     
      hsize_t dims[] = { 1 };
      hsize_t count[] = { 1 };
      hssize_t offset[] = { 0 };
      DataSpace mem(1,dims);
      hssize_t start[] = { 0 };
      hssize_t end[] = { 0 };

      char * data = new char[twidth];
      rtree->Branch("data", data, description.c_str());
     
      for(size_t k = 0; k < nrecs; ++k){
        dsp.selectHyperslab(H5S_SELECT_SET, count, offset, 0, dims);
        ds.read(data, type, mem, dsp);
        rtree->Fill();
        offset[0] += count[0];
      }
      rfile->Write();
    }

