GFLAGS = -g
CFLAGS    = -c -fPIC $(GFLAGS) $(shell root-config --cflags) -I. -I$(HDF5SYS)/include
LFLAGS      = $(GFLAGS) $(shell root-config --libs) -lMinuit -L$(HDF5SYS)/lib -lhdf5 -lhdf5_cpp
CXXFLAGS =-O3 -std=c++0x -pg -D_DEBUG -g -c -Wall

PF : bin/PulseFinding.exe

AN : bin/Analysis.exe

PR : bin/process_run.exe

DP : bin/DataPlot.exe

DAN : bin/DarkAnalysis.exe

lib : lib/libSipmAnalysis.so

kei : obj/Keithleymscb.o

VS : bin/VoltageScan.exe

obj/LecroyHdfFile.o : code/LecroyHdfFile.cxx code/LecroyHdfFile.h code/Waveform.h
	g++ -o obj/LecroyHdfFile.o code/LecroyHdfFile.cxx $(CFLAGS)

lib/libSipmAnalysis.so : obj/Waveform.o obj/WaveformProcessor.o obj/LecroyFile.o obj/LecroyHdfFile.o obj/RootDict.o
	g++ -shared -fPIC -o lib/libSipmAnalysis.so $^ $(LFLAGS)

bin/Analysis.exe : obj/Analysis.o obj/ntpAnalysis.o
	g++ -o bin/Analysis.exe obj/Analysis.o obj/ntpAnalysis.o $(LFLAGS)

bin/DarkAnalysis.exe : obj/DarkAnalysis.o obj/DarkNtpAnalysis.o
	g++ -o bin/DarkAnalysis.exe obj/DarkAnalysis.o obj/DarkNtpAnalysis.o $(LFLAGS)

bin/PulseFinding.exe : obj/PulseFinding.o obj/LecroyFile.o obj/LecroyHdfFile.o obj/WaveformProcessor.o obj/Waveform.o
	g++ -o bin/PulseFinding.exe obj/PulseFinding.o obj/LecroyFile.o obj/LecroyHdfFile.o obj/WaveformProcessor.o  obj/Waveform.o $(LFLAGS)

bin/DataPlot.exe : obj/DataPlot.o
	g++ -o bin/DataPlot.exe obj/DataPlot.o $(LFLAGS)

bin/VoltageScan.exe : obj/VoltageScan.o
	g++ -o bin/VoltageScan.exe obj/VoltageScan.o obj/Keithleymscb.o $(LFLAGS)

bin/process_run.exe : obj/process_run.o 
	g++ -o bin/process_run.exe obj/process_run.o $(LFLAGS)

obj/Analysis.o : code/Analysis.cxx code/ntpAnalysis.h
	g++ -o obj/Analysis.o code/Analysis.cxx $(CFLAGS)

obj/DarkAnalysis.o : code/DarkAnalysis.cxx code/DarkNtpAnalysis.h
	g++ -o obj/DarkAnalysis.o code/DarkAnalysis.cxx $(CFLAGS)

obj/ntpAnalysis.o : code/ntpAnalysis.cxx code/ntpAnalysis.h
	g++ -c -o obj/ntpAnalysis.o $(CFLAGS) code/ntpAnalysis.cxx

obj/DarkNtpAnalysis.o : code/DarkNtpAnalysis.cxx code/DarkNtpAnalysis.h
	g++ -c -o obj/DarkNtpAnalysis.o $(CFLAGS) code/DarkNtpAnalysis.cxx

obj/Keithleymscb.o : code/Keithleymscb.cpp code/Keithleymscb.hpp
	g++ -c -o obj/Keithleymscb.o $(CXXFLAGS) code/Keithleymscb.cpp

obj/PulseFinding.o : code/PulseFinding.cxx code/Waveform.h code/LecroyFile.h code/LecroyHdfFile.h code/WaveformProcessor.h
	g++ -o obj/PulseFinding.o code/PulseFinding.cxx $(CFLAGS)

obj/Waveform.o : code/Waveform.cxx code/Waveform.h 
	g++ -o obj/Waveform.o code/Waveform.cxx $(CFLAGS)

obj/process_run.o : code/process_run.cxx  
	g++ -o obj/process_run.o code/process_run.cxx $(CFLAGS)

obj/DataPlot.o : code/rawData_plotter.cxx
	g++ -o obj/DataPlot.o code/rawData_plotter.cxx $(CFLAGS)

obj/VoltageScan.o : code/VoltageScan.cxx
	g++ -o obj/VoltageScan.o code/VoltageScan.cxx $(CXXFLAGS)

obj/LecroyFile.o : code/LecroyFile.cxx code/LecroyFile.h code/DataFile.h code/Waveform.h
	g++ -o obj/LecroyFile.o code/LecroyFile.cxx $(CFLAGS)

obj/WaveformProcessor.o : code/WaveformProcessor.cxx code/LecroyFile.h
	g++ -c -o obj/WaveformProcessor.o $(CFLAGS) code/WaveformProcessor.cxx

obj/RootDict.o : code/Waveform.h code/LecroyFile.h code/LecroyHdfFile.h code/WaveformProcessor.h code/Linkdef.h
	rootcint -f code/RootDict.cxx -c -I$(HDF5SYS)/include $^
	g++ -c -o$@ code/RootDict.cxx $(CFLAGS)

clean:
	rm -f obj/*.o bin/*.exe lib/*.so
