#include<cstdlib>
#include<fstream>

int RunLookup(int temp, bool DN);

//par[0] = tau, par[1] = p_ap, par[2] = tau_ap, par[3] = p_dn
double fitFunction2(double *x, double *par)
{
	double arg1 = 0, arg2 = 0, arg3 = 0, fitval = 0;
	if((par[0] != 0) && (par[2] != 0)) arg1 = 1/par[0] + 1/par[2];
	if(par[0] != 0) arg2 = 1/par[0];
	if(par[2] != 0) arg3 = 1/par[2];
	
	fitval = par[1]*par[3]*exp(-1*x[0]*arg1)*arg1 + par[1]*arg3*exp(-1*x[0]*arg3)*(1-par[3]) + par[3]*arg2*exp(-1*x[0]*arg2)*(1-par[1]);

	return fitval;
}

void CombineHistos(int numRuns, int temp[])
{
	char filename [14] [100];
	char dataname [14] [100];
	double parameters [14] [4];
	TFile f[14];
	TH1D* hist[14];
	TF1* HistFit[14];

	ifstream iFile;

	char savename [100];
	sprintf(savename, "Time Distribution.pdf");

	int DNoise[14];
	int aPulsing[14];
	int style[14] = {20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33};
	int colour[14] = {1, 2, 3, 4, 30, 6, 7, 8, 9, 40, 41, 42, 43, 44};

	for(int i = 0; i < numRuns; i++)
	{
		DNoise[i] = RunLookup(temp[i], true);
		aPulsing[i] = RunLookup(temp[i], false);
		sprintf(filename[i], "./Timing Distributions/TimeDist-%i-%i.root", DNoise[i], aPulsing[i]);
		sprintf(dataname[i], "./Timing Distributions/TimeDistPars-%i-%i.txt", DNoise[i], aPulsing[i]);	
	}

	TCanvas *c1 = new TCanvas("Timing Distribution", "Timing Distribution", 900, 700);
	c1->SetLogx();
	c1->SetLogy();

	char buffer[100];

	for(int i = 0; i < numRuns; i++)
	{
		f[i] = new TFile(filename[i]);
		hist[i] = (TH1D*)f[i].Get("Timing Distribution");

		iFile.open(dataname[i]);
		iFile >> buffer >> buffer;
		parameters[i][0] = atof(buffer);
		iFile >> buffer >> buffer;
		parameters[i][1] = atof(buffer);
		iFile >> buffer >> buffer;
		parameters[i][2] = atof(buffer);
		iFile >> buffer >> buffer;
		parameters[i][3] = atof(buffer);
		iFile.close();
	}

	char histname[10];
	hist[0]->SetName("hist1");
	hist[0]->SetMarkerStyle(style[i]);
	hist[0]->SetMarkerColor(colour[i]);
	hist[0]->Draw("EP");
	hist[0]->GetXaxis()->SetTitle("Time Distribution(ns)");
	hist[0]->GetYaxis()->SetTitle("Counts");
	hist[0]->GetXaxis()->CenterTitle();
	hist[0]->GetYaxis()->CenterTitle();
	hist[0]->SetMaximum(3e-4);
	hist[0]->SetMinimum(1e-6);

	char fitname[10];
	HistFit[0] = new TF1("fit1", fitFunction2, 20, 500000, 4);
	HistFit[0]->FixParameter(0, parameters[0][0]);
	HistFit[0]->FixParameter(1, parameters[0][1]);
	HistFit[0]->FixParameter(2, parameters[0][2]);
	HistFit[0]->FixParameter(3, parameters[0][3]);
	cout << "Fit 0 " <<  parameters[0][0] << " " <<  parameters[0][1] << " " <<  parameters[0][2] << " " <<  parameters[0][3] << endl;
	HistFit[0]->Draw("same");

	for(int i = 1; i < numRuns; i++)
	{
		sprintf(histname, "hist%i", i+1);
		hist[i]->SetName(histname);
		hist[i]->SetMarkerStyle(style[i]);
		hist[i]->SetMarkerColor(colour[i]);
		hist[i]->Draw("same EP");

		sprintf(fitname, "fit%i", i+1);
		HistFit[i] = new TF1(fitname, fitFunction2, 20, 500000, 4);
		HistFit[i]->FixParameter(0, parameters[i][0]);
		HistFit[i]->FixParameter(1, parameters[i][1]);
		HistFit[i]->FixParameter(2, parameters[i][2]);
		HistFit[i]->FixParameter(3, parameters[i][3]);
		cout << "Fit " << i << " "  <<  parameters[0][0] << " " <<  parameters[0][1] << " " <<  parameters[0][2] << " " <<  parameters[0][3] << endl;
		HistFit[i]->Draw("same");
	}

	TLegend* leg = new TLegend(0.7,0.7,0.9,0.9);
	leg->SetHeader("Legend");
	char legentry[10];

	for(int i = 0; i < numRuns; i++)
	{
		sprintf(legentry, "%iC", temp[i]);
		leg->AddEntry(hist[i], legentry, "EP");
	}
	leg->Draw();

	c1->SaveAs(savename);
}

int RunLookup(int temp, bool DN)
{
	int run;
	if(DN) run = 2024 + -1*temp;
	else run = 2026 + -1*temp;
	return run;
}
